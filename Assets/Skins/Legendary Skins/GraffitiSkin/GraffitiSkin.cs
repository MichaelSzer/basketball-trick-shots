﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraffitiSkin : MonoBehaviour, IOnCollision {

	public float timeBetweenDecal = 0.4f;
	public float minimumImpulse = 1f;
	private float timerDecal;
	private DecalPool decalPool;

	void Awake () {
		decalPool = GetComponent<DecalPool> ();
	}

	public void OnCollisionEnter(Collision other){
		
		if(other.collider.gameObject.layer == LayerMask.NameToLayer("Environment") && other.impulse.magnitude >= minimumImpulse)
		{
			timerDecal = timeBetweenDecal;
			decalPool.SpawnDecal(other);
		}
	}

	public void OnCollisionStay(Collision other){

		/*if(other.collider.gameObject.layer == LayerMask.NameToLayer("Environment") && other.impulse.magnitude >= minimumImpulse)
		{
			timerDecal -= Time.deltaTime;

			if(timerDecal <= 0)
			{
				timerDecal = timeBetweenDecal;
				decalPool.SpawnDecal(other);
			}
		}*/		
	}

	public void OnCollisionExit(Collision other){
		// Nothing
	}
}
