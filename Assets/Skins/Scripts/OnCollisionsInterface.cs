﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IOnCollision
{
	void OnCollisionEnter(Collision other);
	void OnCollisionStay(Collision other);
	void OnCollisionExit(Collision other);
}
