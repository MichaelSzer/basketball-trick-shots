﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterialOverTime : MonoBehaviour {

	public List<Material> materials;
	public float timePerMaterial = 1f;

	float timer = 0f;
	int index = 0;

	Renderer renderer;

	void Awake()
	{
		renderer = transform.parent.gameObject.GetComponent<Renderer> ();
	}

	void Update(){

		timer += Time.deltaTime;

		if(timer >= timePerMaterial)
		{
			timer = 0f;
			index++;

			if(index >= materials.Count)
				index = 0;

			renderer.material = materials[index]; 
		}
	}
}
