﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SkinSelect : MonoBehaviour {  

    void OnStart()
    {
        // Load materials from path
        //skinGalaxy = Resources.Load<Material>("Materials/SkinGalaxy");
        //skinGalaxy = (Material)AssetDatabase.LoadAssetAtPath("Assets/Materials/SkinGalaxy.mat", typeof(Material));  
    }

    public bool changeScale = true;

    private void OnEnable()
    {
        Transform parent = transform.parent;
        transform.parent = null;

        if(changeScale)
            transform.localScale = new Vector3(1, 1, 1);
        
        transform.parent = parent;

        Material material;

        string skin = PlayerPrefs.GetString("SkinSelected", "SkinDefault");

        SkinItem skinItem = DataManager.Instance.GetSkinDictionary(skin);
        material = Resources.Load<Material>(skinItem.materialUrl);

        gameObject.GetComponent<Renderer>().material = material;

        // Destroy preview special functions
        foreach (Transform child in gameObject.transform)
        {
            Destroy(child.gameObject);
        }

        // Load new special functions
        if(skinItem.specialFunction)
        {
            Instantiate(Resources.Load<GameObject>(skinItem.specialFunctionUrl), gameObject.transform);
        }
    }
}
