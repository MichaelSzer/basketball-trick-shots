﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecalPool : MonoBehaviour {

	public int maxDecals = 30;
	public int randomStep = 3;
	public Gradient colorGradient;

	public enum SplatSizeCalculation {AllRandom, RelativeImpulse};
	public SplatSizeCalculation enumSplatSizeCalculation;

	public List<ParticleSystem> particleSystemList;

	public float minDecalSize = 1f, maxDecalSize = 2f, impulseToSize = 0.01f;
	public float normalOffset = 0f;

	private SplatDecalData[] splatDecalsData;
	private List<ParticleSystem.Particle[]> particles;
	private int splatDecalsIndex = 0, lastParticleIndex = 0;

	void Awake(){

		splatDecalsData = new SplatDecalData[maxDecals];
		for(int i = 0; i < maxDecals; i++)
		{
			splatDecalsData[i] = new SplatDecalData();
		}

		particles = new List<ParticleSystem.Particle[]> ();
		for(int i = 0; i < particleSystemList.Count; i++)
			particles.Add(new ParticleSystem.Particle[maxDecals]);
	}

	void Start(){
		
		// Randomize first step
		lastParticleIndex = Random.Range(0, particleSystemList.Count);
	}

	public void SpawnDecal(Collision collision){

		ConfigureSplatDecalData(collision);
		DisplayParticles();
	}

	void ConfigureSplatDecalData(Collision collision){

		if(splatDecalsIndex >= maxDecals)
			splatDecalsIndex = 0;

		// Configure position and rotation
		ContactPoint contactPoint = collision.contacts[0];
		splatDecalsData[splatDecalsIndex].position = contactPoint.point + contactPoint.normal * normalOffset;
		splatDecalsData[splatDecalsIndex].rotation = Quaternion.LookRotation(contactPoint.normal).eulerAngles; 

		// Configure size
		if(enumSplatSizeCalculation == SplatSizeCalculation.AllRandom)
		{
			splatDecalsData[splatDecalsIndex].size = Random.Range(minDecalSize, maxDecalSize);
		}
		else if(enumSplatSizeCalculation == SplatSizeCalculation.RelativeImpulse)
		{			
			float decalSize = minDecalSize + collision.impulse.magnitude * impulseToSize;
			
			if(decalSize > maxDecalSize)
				decalSize = maxDecalSize;

			splatDecalsData[splatDecalsIndex].size = decalSize; 
		}

		// Configure Color Gradient
		splatDecalsData[splatDecalsIndex].color = colorGradient.Evaluate(Random.Range(0f,1f));

		// Assign it to a random particle system
		lastParticleIndex += Random.Range(1, randomStep + 1);
		lastParticleIndex = lastParticleIndex % particleSystemList.Count;
		splatDecalsData[splatDecalsIndex].indexPS = lastParticleIndex;		

		splatDecalsIndex++;
	}

	void DisplayParticles(){

		for(int i = 0; i < maxDecals; i++)
		{
			particles[splatDecalsData[i].indexPS][i].position = splatDecalsData[i].position;
			particles[splatDecalsData[i].indexPS][i].rotation3D = splatDecalsData[i].rotation;
			particles[splatDecalsData[i].indexPS][i].startSize = splatDecalsData[i].size;
			particles[splatDecalsData[i].indexPS][i].startColor = splatDecalsData[i].color;
		}

		for(int i = 0; i < particleSystemList.Count; i++)
			particleSystemList[i].SetParticles(particles[i], maxDecals);
	}
	
}
