﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplatDecalData {

	public Vector3 position;
	public Vector3 rotation;
	public float size;
	public Color color;
	public int indexPS;
}
