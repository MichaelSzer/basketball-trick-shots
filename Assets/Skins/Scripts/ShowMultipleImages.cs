﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ShowMultipleImages : MonoBehaviour {

	public List<Sprite> images;
	public float timePerImage = 0.25f;

	float timer = 0f;
	int index = 0;

	Image image;
	SpriteRenderer spriteRenderer;

	void Awake()
	{
		image = GetComponent<Image> ();
		spriteRenderer = GetComponent<SpriteRenderer> ();
	}

	void Update(){

		timer += Time.deltaTime;

		if(timer >= timePerImage)
		{
			timer = 0f;
			index++;

			if(index >= images.Count)
				index = 0;

			if(spriteRenderer != null)
				spriteRenderer.sprite = images[index];

			if(image != null)
				image.sprite = images[index]; 
		}
	}	
}
