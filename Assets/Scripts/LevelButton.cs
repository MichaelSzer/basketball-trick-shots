﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelButton : MonoBehaviour {

    private bool clickable;
    private int max_level, level;
    private Image image;

	void Start () {

        // Check if the level is unlocked or not
        max_level = PlayerPrefs.GetInt("max_level", 1);

        // Get actual level
        int.TryParse(gameObject.name.Substring(5), out level);

        // Get Image Component
        image = gameObject.GetComponent<Image> ();

        if(level <= max_level)
        {
            // Make button available
            clickable = true;
            image.color = new Color(1f, 1f, 1f, 1f);
        }
        else
        {
            // Make button unavailable
            clickable = false;
            image.color = new Color(1f, 1f, 1f, 0.4f);
        }
	}

    public void LoadLevel()
    {
        if (clickable)
        {
            // Mark in the selected cookie that level was selected from menu
            PlayerPrefs.SetInt("selected", 1);
            // Store level selected
            PlayerPrefs.SetInt("selectedLevel", level);

            // Load level selected
            SceneManager.LoadScene("Level" + level.ToString());
        }
    }
}
