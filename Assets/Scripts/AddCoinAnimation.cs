﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddCoinAnimation : MonoBehaviour {

    Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();    
    }

    public void PlayAddCoinAnimation()
    {
        anim.Play("AddCoinAnimation");
    }
}
