﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingWall : MonoBehaviour {


    public float vel, timerLimit;
    public bool Y, Z;
    public float timerToStart = 0f;
    private float timer;
    private Rigidbody rb;

    void Awake()
    {
        // Get Rigidbody
        rb = GetComponent<Rigidbody>();
    }

    void Start()
    {
        // Start moving wall after time has passed
        Invoke("SetVelocity", timerToStart);
    }

    void SetVelocity()
    {
        timer = (timerLimit - timerToStart) / 2;

        // Set intial velocity
        if (Y)
            rb.velocity = new Vector3(0f, vel, 0f);

        if (Z)
            rb.velocity = new Vector3(0f, 0f, vel);
    }

    // Update is called once per frame
    void Update ()
    {
        timer += Time.deltaTime;

        // Change velocity direction
        if(timer >= timerLimit)
        {
            timer = 0f;
            rb.velocity *= -1;
        }
	}
}
