﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControllerLevelSelector : MonoBehaviour {

    private Vector3 lastPosition;
    private float vel;
    public GameObject levelsInterface;
    const float X_OFFSET_LEVELS = -350f;
    const int NUMBER_LEVELS = 19;
    private float xLimitRight;
    private readonly float xLimitLeft = 0f;
    //private readonly float fasterWithTime = 0.5f;
    private readonly float multiplier = 0.03f;

    private void Start()
    {
        xLimitRight = ( NUMBER_LEVELS - 5 ) * X_OFFSET_LEVELS;
    }

    private void Update()
    {
        if (!Input.GetMouseButton(0))
        {
            // Set last position to the current posi
            lastPosition = new Vector3(-1f, -1f, -1f);

            return;
        }

        if (lastPosition == new Vector3(-1f, -1f, -1f))
        {
            // If last position is null it means the user had stopped touching the screen
            // So we have to get a new last position to avoid jumps between taps
            lastPosition = Input.mousePosition;

            return;
        }

        // Get current mouse position
        Vector3 position = Input.mousePosition;

        // Calculate velocity
        float originalVel = (position.x - lastPosition.x) / Time.deltaTime;

        // Calculate a new velocity with the new multiplier
        vel = originalVel * multiplier;

        // Store this position as last one
        lastPosition = position;

        // If mouse is being held down move interface
        levelsInterface.GetComponent<RectTransform>().anchoredPosition += new Vector2(vel, 0f);

        // Check if levels interface is outside any of the boundaries
        if (levelsInterface.GetComponent<RectTransform>().anchoredPosition.x > xLimitLeft)
            levelsInterface.GetComponent<RectTransform>().anchoredPosition = new Vector2(xLimitLeft, 0f);

        if (levelsInterface.GetComponent<RectTransform>().anchoredPosition.x < xLimitRight)
            levelsInterface.GetComponent<RectTransform>().anchoredPosition = new Vector2(xLimitRight, 0f);
    }
}
