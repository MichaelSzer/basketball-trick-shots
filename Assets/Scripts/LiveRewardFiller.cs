﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class LiveRewardFiller : MonoBehaviour {

    public GameObject mask;
    public float fillerSpeed = 0.2f, plummetSpeed = -0.2f;
    public float yStart, yEnd;

    private bool fill = false, plummet = false;
    private float[] yFillPosition;
    private float yFill;
    private bool runExtraLifeAnimation = false;

    private Brain brain;

    private int extraLifeStep;

    private AudioSource audioSource;
    public float fillVolume = 1;
    public AudioClip fillOne, fillTwo, fillThree;

    private void Awake()
    {
        try
        {
            // Get Brain
            brain = GameObject.Find("Brain").GetComponent<Brain>();
        }
        catch (System.NullReferenceException e)
        {
            // There must be an ui animation to start the level
            Debug.LogException(e);
            Debug.LogError("There must be the Brain component with the script attached.");
        }

        audioSource = GetComponent<AudioSource> ();
    }

    // Use this for initialization
    void Start () {

        // Start array
        yFillPosition = new float[3];
        yFillPosition[0] = yStart + (yEnd - yStart) * (float)0.33;
        yFillPosition[1] = yStart + (yEnd - yStart) * (float)0.66;
        yFillPosition[2] = yEnd;

        StartingStatus();
	}
	
	// Update is called once per frame
	void Update () {

        if (fill)
        {
            //Debug.Log(mask.transform.localPosition.y.ToString() + " " + yFill.ToString());
            if (mask.transform.localPosition.y < yFill)
                mask.transform.Translate(new Vector3(0f, fillerSpeed * Time.deltaTime, 0f));
            else
                fill = false;
        }
        else if (plummet)
        {
            if (mask.transform.localPosition.y > yStart)
                mask.transform.Translate(new Vector3(0f, plummetSpeed * Time.deltaTime, 0f));
            else
                plummet = false;
        }

        if(mask.transform.localPosition.y > yFill && runExtraLifeAnimation)
        {
            ExtraLifeAnimation();
            runExtraLifeAnimation = false;
        }
	}

    public void LoseLife()
    {
        plummet = true;
        extraLifeStep = 0;
        PlayerPrefs.SetInt("extra_life_step", 0);
    }

    public void LevelWon()
    {
        extraLifeStep++;

        PlayerPrefs.SetInt("extra_life_step", extraLifeStep);

        switch(extraLifeStep)
        {
            case 1:
                ShowOne();
                break;
            case 2:
                ShowTwo();
                break;
            case 3:
                ShowThree();
                break;
        }
    }

    void StartingStatus()
    {
        extraLifeStep = PlayerPrefs.GetInt("extra_life_step", 0);

        if (extraLifeStep == 1)
            StartOne();
        else if (extraLifeStep == 2)
            StartTwo();
    }

    void StartOne()
    {
        mask.transform.Translate(new Vector3(0f, yFillPosition[0] - yStart, 0f));
    }

    void StartTwo()
    {
        mask.transform.Translate(new Vector3(0f, yFillPosition[1] - yStart, 0f));
    }

    void ShowOne()
    {
        /*audioSource.Stop();
        audioSource.clip = fillOne;
        audioSource.Play();*/

        //Debug.Log("ShowOne()");
        yFill = yFillPosition[0];
        fill = true;
    }

    void ShowTwo()
    {
        /*audioSource.Stop();
        audioSource.clip = fillTwo;
        audioSource.Play();*/

        //Debug.Log("ShowTwo()");
        yFill = yFillPosition[1];
        fill = true;
    }

    void ShowThree()
    {

        audioSource.Stop();
        audioSource.clip = fillThree;
        audioSource.Play();

        //Debug.Log("ShowThree()");
        yFill = yFillPosition[2];
        fill = true;

        // Reset steps
        PlayerPrefs.SetInt("extra_life_step", 0);

        // Add extra life
        int lives = PlayerPrefs.GetInt("lives", 0);
        lives++;
        PlayerPrefs.SetInt("lives", lives);

        runExtraLifeAnimation = true;
    }

    void ExtraLifeAnimation()
    {
        // TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO
        plummet = true;
        brain.ExtraLifeAnimation();
    }
}
