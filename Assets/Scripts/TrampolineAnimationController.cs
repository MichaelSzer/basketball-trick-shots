﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrampolineAnimationController : MonoBehaviour {

	private Animation animation;

	// Use this for initialization
	void Awake () {
		
		GameObject saltadora = transform.Find("Saltadora").gameObject;

		if(saltadora == null)
			Debug.LogError("'Trampoline' must have a child called 'Saltadora'.");

		// Get Animator from Trampoline
		animation = saltadora.GetComponent<Animation> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown("1")){
			animation.Play();
		}
	}
}
