﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoopFireworks : MonoBehaviour {

    private GameObject fireworksGameObject;
    private GameObject fireworks2GameObject;
    private ParticleSystem fireworks;
    private ParticleSystem fireworks2;

    void Awake ()
    {

        // Get both fireworks gameobjects
        int count = gameObject.transform.childCount;

        for (int i = 0; i < count; i++)
        {
            if (gameObject.transform.GetChild(i).name == "Fireworks")
                fireworksGameObject = gameObject.transform.GetChild(i).gameObject;
            else if (gameObject.transform.GetChild(i).name == "Fireworks2")
                fireworks2GameObject = gameObject.transform.GetChild(i).gameObject;
        }

        // Get Particle System from each fireworks
        fireworks = fireworksGameObject.transform.GetChild(0).GetComponent<ParticleSystem> ();
        fireworks2 = fireworks2GameObject.transform.GetChild(0).GetComponent<ParticleSystem> ();
    }

    public void PlayFireworks()
    {
        fireworks.Play();
        fireworks2.Play();
    }
}
