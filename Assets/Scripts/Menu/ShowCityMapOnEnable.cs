﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowCityMapOnEnable : MonoBehaviour {

	void OnEnable()
	{
		if(MenuBackgroundManager.Instance != null)
			MenuBackgroundManager.Instance.ShowCityMap();
	}
}
