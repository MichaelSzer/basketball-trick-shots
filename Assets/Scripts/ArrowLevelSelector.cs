﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowLevelSelector : MonoBehaviour {

    public float originalVel = 20f;
    private float vel;
    public GameObject levelsInterface;
    const float X_OFFSET_LEVELS = -350f;
    const int NUMBER_LEVELS = 10;
    private float xLimitRight;
    private float xLimitLeft = 0f;
    private bool moving;
    private float fasterWithTime = 0.5f;
    private float multiplier = 1f;

    private void Start()
    {
        xLimitRight = ( NUMBER_LEVELS - 5 ) * X_OFFSET_LEVELS;
    }

    public void startMoving ()
    {
        moving = true;

        // Reset multiplier
        multiplier = 1f;
	}

    public void stopMoving()
    {
        moving = false;
    }

    private void Update()
    {
        // If mouse is not clicking return
        if (!moving)
            return;

        // Make the velocity bigger for each update
        multiplier += (fasterWithTime * Time.deltaTime);

        // Calculate a new velocity with the new multiplier
        vel = originalVel * multiplier;

        levelsInterface.GetComponent<RectTransform>().anchoredPosition += new Vector2(vel*Time.deltaTime, 0f);

        // Check if levels interface is outside any of the boundaries
        if (levelsInterface.GetComponent<RectTransform>().anchoredPosition.x > xLimitLeft)
            levelsInterface.GetComponent<RectTransform>().anchoredPosition = new Vector2(xLimitLeft, 0f);

        if (levelsInterface.GetComponent<RectTransform>().anchoredPosition.x < xLimitRight)
            levelsInterface.GetComponent<RectTransform>().anchoredPosition = new Vector2(xLimitRight, 0f);
    }
}
