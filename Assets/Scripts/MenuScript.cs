﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class MenuScript : MonoBehaviour {

    public GameObject mainMenu;
    public GameObject levelSelector;
    public GameObject shop;
    public GameObject levelsInterface;
    public GameObject inventory;
    public GameObject gameModeSelector;
    const float X_OFFSET_LEVEL = -350f;
    private bool selected;          // Is it coming from a level selected in the main menu?
    private int levelSelected;
    public int NUMBER_LEVELS = 10;

    private void OnEnable()
    {
       CheckTutorial();
       SoundtrackManager.Instance.Play("menu");       
    }

    private void Start()
    {
        SelectPack();
        //GetCoins(5000);
    }

    bool CheckTutorial()
    {
        // ----------- Show tutorial pack until it is completed ------------------- 
        float tutorialCompleted = PlayerPrefs.GetFloat("TutorialCompleted_Todo", 0f);

        if(tutorialCompleted != 100f)
        {
            SoundtrackManager.Instance.Play("city");
            SceneManager.LoadScene("Tutorial");
            return true;
        }        
        // ------------------------------------------------------------------------ 
    
        return false;
    }

    void GetCoins(int coins)
    {
        string res = PlayerPrefs.GetString("FirstTime", "No");

        if (res == "Yes")
            return;

        PlayerPrefs.SetString("FirstTime", "Yes");

        PlayerPrefs.SetInt("coins", coins);
    }

    void SelectPack()
    {

        float tutorialCompleted = PlayerPrefs.GetFloat("TutorialCompleted_Todo", 0f);

        if(tutorialCompleted != 100f){
            return;
        }

        int firstTimeTutorialCompleted = PlayerPrefs.GetInt("FirstTimeTutorialCompleted", 0);

        if(firstTimeTutorialCompleted > 1){

            string pack = PlayerPrefs.GetString("PackSelected", "");
            
            if (pack != "")
                ShowPackSelector();
            else
                SoundtrackManager.Instance.Play("menu");
        }
        else if(firstTimeTutorialCompleted == 1)
        {
            PlayerPrefs.SetInt("FirstTimeTutorialCompleted", 2);
            SoundtrackManager.Instance.Play("menu");
        }
    }

    public void ResetGame()
    {
        int startingLifes = PlayerPrefs.GetInt("StartingLives", 3);

        // Refill lives
        PlayerPrefs.SetInt("lives", startingLifes);

        // Mark that level is being play on "history mode"
        PlayerPrefs.SetInt("selected", 0);

        // Reset InGame cookies
        PlayerPrefs.SetInt("extra_life_step", 0);

        // Send an analytics saying the play button was clicked
        int clicked = PlayerPrefs.GetInt("clicked_play_button", 0);
        if(clicked == 0)
        {
            PlayerPrefs.SetInt("clicked_play_button", 1);
            Analytics.CustomEvent("clicked_play_button");
        }

        // Get last checkpoint
        int levelCheckpoint = PlayerPrefs.GetInt("checkpoint", 1);
        
        if(levelCheckpoint == NUMBER_LEVELS + 1)
        {
            PlayerPrefs.SetInt("checkpoint", 1);
            levelCheckpoint = 1;
        }

        // Load last checkpoint
        SceneManager.LoadScene("Level" + levelCheckpoint.ToString());
    }

    public void GoToMainMenu()
    {
        // Destroy AudioManager from gameplay
        Destroy(GameObject.Find("AudioManager"));

        // Load Main Menu
        SceneManager.LoadScene("MainMenu");
    }

    void HideAll()
    {
        mainMenu.SetActive(false);
        levelSelector.SetActive(false);
        gameModeSelector.SetActive(false);
        shop.SetActive(false);
        inventory.SetActive(false);
    }

    public void ShowShop()
    {
        HideAll();
        shop.SetActive(true);
    }

    public void ShowInventory()
    {
        HideAll();
        inventory.SetActive(true);
    }

    public void ShowPackSelector()
    {
        // Send an analytics saying the level selector button was clicked
        int clicked = PlayerPrefs.GetInt("clicked_level_selector_button", 0);
        if (clicked == 0)
        {
            PlayerPrefs.SetInt("clicked_level_selector_button", 1);
            Analytics.CustomEvent("clicked_level_selector_button");
        }

        HideAll();

        // Show Level Selector
        levelSelector.SetActive(true);
    }

    public void ShowModeSelector()
    {
        mainMenu.SetActive(false);
        gameModeSelector.SetActive(true);
    }

    public void BackToMenuFromModeSelector()
    {
        mainMenu.SetActive(true);
        gameModeSelector.SetActive(false);   
    }

    public void LoadArcadeMode()
    {
        SceneManager.LoadScene("ArcadeGame");
    }

    public void ShowMainMenu()
    {
        HideAll();

        // Show Main Menu
        mainMenu.SetActive(true);

        SoundtrackManager.Instance.Play("menu");
    }
}
