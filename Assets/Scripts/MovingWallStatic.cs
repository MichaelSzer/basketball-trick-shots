﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingWallStatic : MonoBehaviour {

    public float vel, timerLimit;
    public bool Y, Z;
    public float timerToStart = 0f;
    private float timer;
    private Transform tr;
    private bool started = false;

    void Awake()
    {
        // Get Transform
        tr = gameObject.transform;
    }

    void Start()
    {
        // Start moving wall after time has passed
        Invoke("StartUpdate", timerToStart);

        // Initial timer 
        timer = (timerLimit - timerToStart) / 2;
    }

    void StartUpdate()
    {
        started = true;
    }

    void Move()
    {
        // Set intial velocity
        if (Y)
            tr.Translate(new Vector3(0f, vel*Time.deltaTime, 0f));

        if (Z)
            tr.Translate(new Vector3(0f, 0f, vel * Time.deltaTime));
    }

    // Update is called once per frame
    void Update ()
    {
        if (!started)
            return;

        timer += Time.deltaTime;

        // Change velocity direction
        if(timer >= timerLimit)
        {
            timer = 0f;
            vel *= -1;
        }

        Move();
	}
}
