﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCameraTrigger : MonoBehaviour {

    new public GameObject camera;

    private Vector3 startedPos;
    private Vector3 startedRot;

    public Vector3 speed;
    public Vector3 angularSpeed;

    public Vector3 move;
    public Vector3 rotate;

    bool activated = false;

    private void Start()
    {
        startedPos = camera.transform.position;
        startedRot = camera.transform.rotation.eulerAngles;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (activated || other.name != "MainPlayer")
            return;

        activated = true;

        StartCoroutine(change());
    }

    private Vector3 moved;
    private Vector3 rotated;

    IEnumerator change()
    {
        while (GreaterThan(move, moved) || GreaterThan(rotate, rotated))
        {
            if (move.x > moved.x)
                moved.x += speed.x * Time.deltaTime;
            else
                moved.x = move.x;

            if (move.y > moved.y)
                moved.y += speed.y * Time.deltaTime;
            else
                moved.y = move.y;

            if (move.z > moved.z)
                moved.z += speed.z * Time.deltaTime;
            else
                moved.z = move.z;

            if (rotate.x > rotated.x)
                rotated.x += angularSpeed.x * Time.deltaTime;
            else
                rotated.x = rotate.x;

            if (rotate.y > rotated.y)
                rotated.y += angularSpeed.y * Time.deltaTime;
            else
                rotated.y = rotate.y;

            if (rotate.z > rotated.z)
                rotated.z += angularSpeed.z * Time.deltaTime;
            else
                rotated.z = rotate.z;

            camera.transform.SetPositionAndRotation(startedPos + moved, Quaternion.Euler(startedRot + rotated));

            Debug.Log(Time.time);

            yield return null;
        }        
    }

    bool GreaterThan(Vector3 v1, Vector3 v2)
    {
        if (Mathf.Abs(v1.x) > Mathf.Abs(v2.x) || Mathf.Abs(v1.y) > Mathf.Abs(v2.y) || Mathf.Abs(v1.z) > Mathf.Abs(v2.z))
            return true;
        else
            return false;
    }

    public void Restart()
    {
        activated = false;
        rotated = new Vector3(0, 0, 0);
        moved = new Vector3(0, 0, 0);
        Debug.Log("Restart()");
    }

    Vector3 Multiply(Vector3 v1, Vector3 v2)
    {
        return new Vector3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
    }
}
