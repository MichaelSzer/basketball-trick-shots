﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

	public AudioClip tracksound;
	public List<BounceEffect> fxBounce;
	public Audio fxVictory;  
	public Audio fxTrampoline;
	private AudioSource audioSourceTrack;
	private AudioSource audioSourceFX;
	private AudioSource audioSourceBounce;

	public bool dontDestroyOnLoad = true;

	private SmoothPlayAudio smoothPlayAudio;

	[Range(0f, 1f)]
	public float trackSoundVolume = 1f;
	public float trackSmoothTime = 2f;

	// Use this for initialization
	void Awake () {

		// Destroy game object if it is duplicated
		bool duplicated = CheckIfDuplicate();
		if(duplicated)
			Destroy(gameObject);

		if(dontDestroyOnLoad)
			DontDestroyOnLoad(gameObject);

		// Get Smooth Audio Script
		smoothPlayAudio = GetComponent<SmoothPlayAudio> ();

		if(tracksound != null){

			if(CheckSoundtrack())
			{
				// Create audio source track with clip and play it
				audioSourceTrack = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
				audioSourceTrack.clip = tracksound;
				audioSourceTrack.loop = true;	// Lopp songtrack
				smoothPlayAudio.SmoothAudio(audioSourceTrack, trackSoundVolume, trackSmoothTime);
			}
			
		}else{
			Debug.Log("var 'soundtrack' not declared, not playing music.");
		}

		// Create audio source track
		audioSourceFX = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
		audioSourceBounce = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;

		// Sort all bound effect from threshold
		fxBounce.Sort(SortBounceEffect);
	}
	
	bool CheckIfDuplicate(){

		GameObject[] dontDestroyOnLoadObjects = GetDontDestroyOnLoadObjects.function();
		//Debug.Log("DontDestroyOnLoad GameObjects: " + dontDestroyOnLoadObjects.Length.ToString());

		foreach(GameObject gb in dontDestroyOnLoadObjects){
			if(gb.name == "AudioManager")
				return true;
		}

		return false;
	}

	bool CheckSoundtrack(){

		return GameObject.Find("SoundtrackManager") == null;
	}

	void ConfigureAudioSource(AudioSource audioSource){
		// Configure generic audio
	} 

	int SortBounceEffect(BounceEffect bounceEffect1, BounceEffect bounceEffect2){
		return bounceEffect2.threshold.CompareTo(bounceEffect1.threshold);
	}

//	-------------------------- Public Methods --------------------------------

	public void PlayVictorySound () {
		
		// Play the victory clip into the audio source fx
		audioSourceFX.PlayOneShot(fxVictory.audioClip, fxVictory.volume);
	}

	public void PlayBounceEffect(Vector3 relativeVelocity){

		float magnitude = relativeVelocity.magnitude;

		foreach(BounceEffect bounceEffect in fxBounce){

			// If the magnitude is bigger than the threshold play sound effect
			if(magnitude > bounceEffect.threshold){

				audioSourceBounce.PlayOneShot(bounceEffect.audio.audioClip, bounceEffect.audio.volume);
			}
		}
	}

	public void PlayTrampolineEffect(){

		audioSourceFX.PlayOneShot(fxTrampoline.audioClip, fxTrampoline.volume);
	}
}
