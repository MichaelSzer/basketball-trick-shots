﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothPlayAudio : MonoBehaviour {

	void CheckParameters (float volume, float timeSmooth) {

		if(volume > 1 || 0 > volume){
			Debug.LogError("var 'volume' must have a value between 0 and 1.");
			return;
		}

		if(timeSmooth < 0){
			Debug.LogError("var 'timeSmooth' must have a value bigger than or equal to 0.");
			return;	
		}
	}

	public void SmoothAudio(AudioSource audioSource, float volume, float timeSmooth){

		// Debug line
		//Debug.Log("Smooth audio: " + audioSource.clip.name + " , volume " + volume + " , timeSmooth " + timeSmooth + " .");

		// Start audio
		audioSource.Play();

		// Check parameters
		CheckParameters(volume, timeSmooth);

		// Start smooth transition
		StartCoroutine(TransitionAudio(audioSource, volume, timeSmooth));
	}

	IEnumerator TransitionAudio(AudioSource audioSource, float volume, float timeSmooth){

		float startTime = Time.time;
		float timer = 0f;

		while(timer <= timeSmooth){
			
			// Update timer
			timer = Time.time - startTime;

			// Used to raise the volume along with the time
			float transition = timer / timeSmooth;

			// Cant be a number bigger than 1
			if(transition >= 1)
				transition = 1f;

			float vol = Mathf.Lerp(0f, volume, transition);
			audioSource.volume = vol;

			yield return new WaitForSeconds(0.1f);
		}

		audioSource.volume = volume;

		yield return 0;
	}
}
