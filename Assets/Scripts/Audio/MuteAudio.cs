﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteAudio : MonoBehaviour {

	public Sprite muteSpriteOn, muteSpriteOff;
	Sprite actualSprite;

	Image image;

	// Use this for initialization
	void Start () {
		
		int mute = PlayerPrefs.GetInt("mute", 0);

		if(mute == 1)
		{
			AudioListener.volume = 0f;
			actualSprite = muteSpriteOff;
		}
		else
		{
			AudioListener.volume = 1f;
			actualSprite = muteSpriteOn;
		}

		// Get image of the button
		image = GetComponent<Image> ();

		SetMuteButtonImage();
	}

	public void ToggleMute(){

		int mute = PlayerPrefs.GetInt("mute", 0);

		if(mute == 1)
		{
			PlayerPrefs.SetInt("mute", 0);
			AudioListener.volume = 1f;
			actualSprite = muteSpriteOn;
		}
		else
		{
			PlayerPrefs.SetInt("mute", 1);
			AudioListener.volume = 0f;
			actualSprite = muteSpriteOff;
		}

		SetMuteButtonImage();
	}

	void SetMuteButtonImage(){

		image.sprite = actualSprite;
	}
}
