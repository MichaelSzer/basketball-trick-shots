﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsInGameManager : MonoBehaviour {

	public static SoundsInGameManager Instance;

	public List<BounceEffect> fxBounce;
	public Audio fxVictory;  
	public Audio fxTrampoline;
	public Audio fxWhistle;
	private AudioSource audioSourceFX;
	private AudioSource audioSourceBounce;
	private AudioSource audioSourceWhistle;

	// Ball Shoots results audio
	private AudioSource audioSourceBallShoots;
	public List<Audio> fxTriplePoint;
	public List<Audio> fxDoublePoint;
	public List<Audio> fxOnePoint;
	public List<Audio> fxOutsideShoot;
	public List<Audio> fxRandomComments;

	public float randomCommentRate = 5f;
	private float randomCommentTimer = 0f;

	// Use this for initialization
	void Awake () {

		Instance = this;

		// Create audio source track
		audioSourceFX = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
		audioSourceBounce = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
		audioSourceBallShoots = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
		audioSourceWhistle = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;

		// Sort all bound effect from threshold
		fxBounce.Sort(SortBounceEffect);
	}

	void Update(){

		randomCommentTimer += Time.deltaTime;

		if(randomCommentTimer > randomCommentRate)
			PlayRandomComment();
	}

	void ConfigureAudioSource(AudioSource audioSource){
		// Configure generic audio
	} 

	int SortBounceEffect(BounceEffect bounceEffect1, BounceEffect bounceEffect2){
		return bounceEffect2.threshold.CompareTo(bounceEffect1.threshold);
	}

//	-------------------------- Public Methods --------------------------------

	public void PlayVictorySound () {
		
		// Play the victory clip into the audio source fx
		audioSourceFX.PlayOneShot(fxVictory.audioClip, fxVictory.volume);
	}

	public void PlayWhistle(){
		audioSourceWhistle.PlayOneShot(fxWhistle.audioClip, fxWhistle.volume);
	}

	public void PlayBounceEffect(Vector3 relativeVelocity){

		float magnitude = relativeVelocity.magnitude;

		foreach(BounceEffect bounceEffect in fxBounce){

			// If the magnitude is bigger than the threshold play sound effect
			if(magnitude > bounceEffect.threshold){

				audioSourceBounce.PlayOneShot(bounceEffect.audio.audioClip, bounceEffect.audio.volume);
			}
		}
	}

	public void PlayTrampolineEffect(){
		audioSourceFX.PlayOneShot(fxTrampoline.audioClip, fxTrampoline.volume);
	}

	void BallShoot(List<Audio> soundsList)
	{
		randomCommentTimer = 0f;

		int bound = soundsList.Count;
		int random = Random.Range(0, bound);

		audioSourceFX.PlayOneShot(soundsList[random].audioClip, soundsList[random].volume);
	}

	public void PlayTriplePoint(){

		BallShoot(fxTriplePoint);
	}

	public void PlayDoublePoint(){

		BallShoot(fxDoublePoint);
	}

	public void PlayOnePoint(){

		BallShoot(fxOnePoint);
	}

	public void PlayOutsideShoot(){

		BallShoot(fxOutsideShoot);
	}

	public void PlayRandomComment(){

		BallShoot(fxRandomComments);
	}
}
