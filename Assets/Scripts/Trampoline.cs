﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampoline : MonoBehaviour {
    
	private Brain brain;

	void Awake()
	{
		brain = GameObject.Find("Brain").GetComponent<Brain> ();
	}

	void OnCollisionExit (Collision other)
    {
        // If hitted by the main player, change his direction to the other side and speed it up
	    if(other.gameObject.name == "MainPlayer")
        {
            Rigidbody rb = other.gameObject.GetComponent<Rigidbody> ();
            rb.velocity *= 1.2f; 

            // Tell Brain
            brain.BouncingTrampoline();
        }   
	}
}
