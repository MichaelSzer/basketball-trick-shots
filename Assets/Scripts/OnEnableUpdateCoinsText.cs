using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnEnableUpdateCoinsText : MonoBehaviour
{

	void OnEnable()
	{
		int coins = PlayerPrefs.GetInt("coins", 0);
		GetComponent<Text> ().text = coins.ToString();
	}
}