using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DisplayAdsInTime : MonoBehaviour 
{
	public static DisplayAdsInTime Instance;

	private float timeBetweenAds;	// In Seconds
	private string nextScene;

	void Awake()
	{
		Instance = this;
	}

	void Start()
	{
		timeBetweenAds = RemoteTunningVariables.Instance.TimeToDisplayAds;
		//Debug.Log("TimeToDisplayAd: " + timeBetweenAds.ToString());
	}

	protected void Update()
	{
		//Store time
		float timePlayed = PlayerPrefs.GetFloat("timePlayed", 0f);
		timePlayed += Time.deltaTime;
		PlayerPrefs.SetFloat("timePlayed", timePlayed);
	}


	void SuccessfullCallback(){
		PlayerPrefs.SetFloat("timePlayed", 0f); // Reset timer if ad was shown
	}

	void FailureCallback(){}

	void FailureNextSceneCallback()
	{
		SceneManager.LoadScene(nextScene);
	}	

	void SuccessfullNextSceneCallback()
	{
		PlayerPrefs.SetFloat("timePlayed", 0f); // Reset timer if ad was shown
		SceneManager.LoadScene(nextScene);
	}

	public void FinishPlaying(string mode = "none", Dictionary<string, string> parameters = null)
	{
		int adsRemover = PlayerPrefs.GetInt("adsRemover", 0);

		if(adsRemover != 0)
			return; 

		float timePlayed = PlayerPrefs.GetFloat("timePlayed", 0f);

		//Debug.Log("Time played with ads: " + timePlayed.ToString());

		if(parameters != null)
			nextScene = parameters["nextScene"];

		if(timePlayed > timeBetweenAds)
		{
			// Display ad and reset timer
			string typeOfAd = "interstitialVideo";

			if(mode == "NextScene")
			{
				AdsManager.Instance.ReproduceAd(SuccessfullNextSceneCallback, FailureNextSceneCallback, typeOfAd);
			}
			else
			{
				AdsManager.Instance.ReproduceAd(SuccessfullCallback, FailureCallback, typeOfAd);
			}
		}
		else if(mode == "NextScene")
		{
			FailureNextSceneCallback();
		}
	}
}