﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Monetization;
using GoogleMobileAds.Api;

public class AdsManager : MonoBehaviour {

    public static AdsManager Instance;

    string gameId = "2913905";
    bool testMode = false;

    private RewardBasedVideoAd rewardBasedVideoAdmob;
    private InterstitialAd interstitialAd;

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        RequestInterstitialAdmob();
    }

    void OnSceneUnloaded(Scene scene)
    {
        if(interstitialAd != null)
            interstitialAd.Destroy();
    }

    void Awake()
    {
        if(CheckIfDuplicate.function("AdsManager"))
            Destroy(gameObject);

        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {   
        // ---------------------- Set Events ----------------------
        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.sceneUnloaded += OnSceneUnloaded;

        InitializeAds();
    }

    void InitializeAds()
    {
        try{

            // ---------------------- Unity Ads -------------------
            if (Application.platform == RuntimePlatform.Android)
                gameId = "2913905";
            else if (Application.platform == RuntimePlatform.IPhonePlayer)
                gameId = "2913907";

            if (!Monetization.isInitialized)
            {
                //Debug.Log("Monetization initializing.");
                Monetization.Initialize(gameId, testMode);
                //Debug.Log("Monetization initlized with the gameid " + gameId.ToString() + " and DebugMode " + testMode.ToString());
            }

            // -------------------- Google Admob ----------------
            string appId = "ca-app-pub-8246082908947605~2496832295";
            MobileAds.Initialize(appId);

            rewardBasedVideoAdmob = RewardBasedVideoAd.Instance;

            rewardBasedVideoAdmob.OnAdClosed += HandleRewardBasedVideoClosed;
            rewardBasedVideoAdmob.OnAdRewarded += HandleRewardBasedVideoRewarded;

            RequestRewardBasedVideoAdmob();
        }
        catch(Exception e){
            Debug.LogError(e);
        }
    }

    string rewardedVideoId = "rewardedVideo";
    string interstitialVideoId = "interstitialVideo";

    public delegate void CallbackFunction();
    CallbackFunction SuccessfulCallback, FailureCallback;

    void RequestRewardBasedVideoAdmob()
    {

        #if UNITY_ANDROID
            string adUnitId = "ca-app-pub-8246082908947605/2267222840";
        #elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-8246082908947605/2267222840";
        #else
            string adUnitId = "unexpected_platform";
        #endif

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded video ad with the request.
        rewardBasedVideoAdmob.LoadAd(request, adUnitId);
    }
    
    void RequestInterstitialAdmob()
    {
        #if UNITY_ANDROID
            string adUnitId = "ca-app-pub-8246082908947605/5069603867";
        #elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-8246082908947605/5069603867";
        #else
            string adUnitId = "unexpected_platform";
        #endif
        
        try{
            
            // Initialize an InterstitialAd.
            interstitialAd = new InterstitialAd(adUnitId);

            interstitialAd.OnAdClosed += HandleInterstitialAdmobClosed;
            interstitialAd.OnAdFailedToLoad += HandleOnAdFailedToLoad;

            AdRequest request = new AdRequest.Builder().Build();
            interstitialAd.LoadAd(request);
        }
        catch(Exception e){
            Debug.LogError(e);            
        }
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        interstitialAd.Destroy();
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        RequestRewardBasedVideoAdmob();
        PauseManager.Instance.Resume();
    }

    public void HandleInterstitialAdmobClosed(object sender, EventArgs args)
    {
        interstitialAd.Destroy();
        RequestInterstitialAdmob();

        SuccessfulCallback();

        PauseManager.Instance.Resume();
    }

    bool ShowUnityAd(string placementId)
    {
        if (!Monetization.IsReady(placementId))
        {
            return false;
        }

        ShowAdPlacementContent ad = null;
        ad = (ShowAdPlacementContent)Monetization.GetPlacementContent(placementId);
        ad.Show(HandleAdResult);

        return true;
    }

    bool ShowGoogleAdMob(string placementId)
    {
        if(placementId == rewardedVideoId){

            if(rewardBasedVideoAdmob == null || !rewardBasedVideoAdmob.IsLoaded())
                return false;

            rewardBasedVideoAdmob.Show();
            return true;
        
        }else if(placementId == interstitialVideoId){

            if(interstitialAd == null || !interstitialAd.IsLoaded())
                return false;

            interstitialAd.Show();
            return true;
        }

        return false;
    }

    bool RequestAd(string placementId)
    {
        if(ShowUnityAd(placementId))
            return true;

        if(ShowGoogleAdMob(placementId))
            return true;

        return false;
    }


    public void ReproduceAd(CallbackFunction _successful, CallbackFunction _failure, string typeOfAd = "")
    {
        if(typeOfAd != interstitialVideoId && typeOfAd != rewardedVideoId)
        {
            Debug.LogError("Requested ad format invalid.");
            return;
        }

        SuccessfulCallback = _successful;
        FailureCallback = _failure;

        try{
            if(RequestAd(typeOfAd))
                PauseManager.Instance.Pause();
            else
                FailureCallback();
        }
        catch(ArgumentException e){
            Debug.LogError(e);
            FailureCallback();
        }
    }

    void HandleAdResult(ShowResult result)
    {
        PauseManager.Instance.Resume();

        switch (result)
        {
            case ShowResult.Finished:
                SuccessfulAd();
                break;
            case ShowResult.Skipped:
                SkippedAd();
                break;
            case ShowResult.Failed:
                FailedAd();
                break;
        }
    }

    void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        RequestRewardBasedVideoAdmob();
        SuccessfulAd();
    }

    void SuccessfulAd()
    {
        //Debug.Log("Ad watched successfully.");
        SuccessfulCallback();
    }

    void SkippedAd()
    {
        //Debug.Log("Ad skipped.");
        SuccessfulCallback();
    }

    void FailedAd()
    {
        //Debug.Log("Ad failed to reproduce.");
        FailureCallback();
    }
}
