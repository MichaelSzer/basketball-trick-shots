﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teletransporter : MonoBehaviour {

    private Transform tr;
    private GameObject particleSystemGameObject;
    private ParticleSystem particleSystem;
    private GameObject otherTele;
    private Transform otherTr;
    private Teletransporter otherTeleScript;
    private bool activate = true; 

    private void Awake()
    {
        // Get Transform
        tr = gameObject.GetComponent<Transform> ();

        // Get Particle System Game Object
        particleSystemGameObject = tr.GetChild(0).gameObject;

        // Get Particle System
        particleSystem = particleSystemGameObject.GetComponent<ParticleSystem> ();

        // Find other teleporter
        if(gameObject.name == "TeletransporterA")
            otherTele = tr.parent.Find("TeletransporterB").gameObject;
        else
            otherTele = tr.parent.Find("TeletransporterA").gameObject;

        // Get Transform
        otherTr = otherTele.transform;

        // Get other teletransporter script
        otherTeleScript = otherTele.GetComponent<Teletransporter> ();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "MainPlayer")
        {
            // Check if teletransporter is ready to used
            if (!activate)
                return;

            // Teletransport ball to other portal
            other.gameObject.transform.position = otherTr.position;

            // Run Particle System and other stuff
            justTeleported();
            otherTeleScript.justTeleported();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "MainPlayer")
        {
            // Activate teletransporter
            activate = true;
        }
    }

    public void justTeleported()
    {
        // Deactivate teletransporter
        activate = false;

        // Play subparticles
        particleSystem.Play();
    }
}
