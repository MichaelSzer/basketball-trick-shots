﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInGameAnimations : MonoBehaviour {

    Animator controller;

	// Use this for initialization
	void Awake () {

        // Get animation controller
        controller = GetComponent<Animator>();
	}
	
	public void ExtraLifeAnimation()
    {
        controller.Play("ExtraLifeAnimation");
    }
}
