﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBackgroundManager : MonoBehaviour {

	private static MenuBackgroundManager _instance;
	public static MenuBackgroundManager Instance{ get { return _instance; } }

	public GameObject cityMap, basketballMap;

	void Awake()
	{
		if(Instance != null)
		{
			Destroy(this.gameObject);
		}
		else
		{
			_instance = this;
		}
	}

	void HideAll()
	{
		cityMap.SetActive(false);
		basketballMap.SetActive(false);
	}

	public void ShowCityMap()
	{
		if(cityMap.activeSelf)
			return;

		HideAll();
		cityMap.SetActive(true);
	}

	public void ShowBasketballMap()
	{
		if(basketballMap.activeSelf)
			return;

		HideAll();
		basketballMap.SetActive(true);
	}
}
