﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

[System.Serializable]
public struct LevelItem 
{
    public GameObject level;
    public GameObject gameCamera;
    public GameObject mainPlayer;
    public HoopFireworks hoopFireworks;
    public LiveRewardFiller liveRewardFiller;
    public List<MoveCameraTrigger> moveCameraTriggers;
    public List<BotonCollider> buttons;
}

[RequireComponent(typeof(DisplayAdsInTime))]
public class Brain : MonoBehaviour {

    #region Variables
    private Transform mpTr;
    private Rigidbody mpRb;
    private BallControlling ballScript;
    private bool tapTimerOnOne = false;
    private float tapTimerOne = 0f;
    private bool tapTimerOnTwo = false;
    private float tapTimerTwo = 0f;
    public bool ballThrowed = false;
    const float TAP_TIMER_ONE_DURATION = 4f;
    const float TAP_TIMER_TWO_DURATION = 2f;
    private int lives;
    private GameObject textSampleGameObject;
    private GameObject livesGameObject;
    private GameObject livesTextGameObject;
    private Text livesText;
    private Text coinsText;
    private bool triggerOne = false;
    private bool triggerTwo = false;
    private bool isOnFloor = false;
    private int livesLost = 0;
    private int level = 1;
    private int coins;
    private bool levelWon = false;
    private bool ready = true;
    private bool selected;             // Was game selected from Main Menu?
    private GameObject lostScreen;
    private int timesTried;
    private AudioManager audioManager;
    private UIInGameAnimations uiAnimation;
    private AddCoinAnimation addCoinAnimation;
    public string percentageCompletedCookie;
    public string packName;
    public int numberOfLevels;
    private GameObject tapToRespawn;
    private Vector3 cameraStartedPos;
    private Vector3 cameraStartedRot;
    public GameObject videoTutorial;
    #endregion

    public List<LevelItem> levelsContainer;

    private void Awake()
    {
        tapToRespawn = GameObject.Find("TapScreenToRespawn");

        // Hide image
        tapToRespawn.SetActive(false);

        // Get Lives Game Object
        livesGameObject = GameObject.Find("Lives");

        // Get LivesText Game Object
        livesTextGameObject = GameObject.Find("LivesText");

        coinsText = GameObject.Find("CoinsText").GetComponent<Text> ();

        // Get actual LivesText text
        livesText = livesTextGameObject.GetComponent<Text> ();

        // Get Lost Screen
        lostScreen = GameObject.Find("LostScreen");

        // Get times tried
        timesTried = PlayerPrefs.GetInt("timesTried_" + (level - (level % 5) + 1).ToString(), 0);

        // Add time tracker to display ads
        if(GetComponent<DisplayAdsInTime> () == null)
            gameObject.AddComponent<DisplayAdsInTime> ();

        try
        {
            // Get Audio Manager Script Reference
            audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager> ();  
        }
        catch(System.NullReferenceException e)
        {
            // There must be an audio manager to start the level
            Debug.LogException(e);
            Debug.LogError("There isn't an 'AudioManager' in the scene. Music neither sound will be played.");
        }

        try
        {
            // Get UI animation
            uiAnimation = GameObject.Find("WholeUIComponents").GetComponent<UIInGameAnimations> ();
        }
        catch (System.NullReferenceException e)
        {
            // There must be an ui animation to start the level
            Debug.LogException(e);
            Debug.LogError("There isn't an 'UIInGameAnimation' in the 'WholeUIComponents'. UI animationneither will not be played.");
        }

        try
        {
            // Get Add Coin Animation
            addCoinAnimation = GameObject.Find("AddCoin").GetComponent<AddCoinAnimation>();
        }
        catch (System.NullReferenceException e)
        {
            // There must be an ui animation to start the level
            Debug.LogException(e);
            Debug.LogError("There isn't an 'AddCoin' in the hoop. Earned coins animation won't be played.");
        }
    }

    private void Start()
    {
        ConfigureLevel();

        ResetLives();
        UpdateLives();
        UpdateCoins();        

        string eventName = packName + " started.";
        AnalyticsManager.Instance.TriggerEvent(eventName);

        if(packName == "Tutorial")
        {
            HideLives();

            int firstTimeTutorial = PlayerPrefs.GetInt("FirstTimeTutorialCompleted", 0);
            if(firstTimeTutorial == 0)
            {
                AnalyticsManager.Instance.TriggerEvent("Tutorial started for the first time.");
            }
        }

        PlayerPrefs.SetString("PackSelected", packName);

        screenCoefficient = alpha / Mathf.Sqrt(Screen.width * Screen.height);
    }

    void ResetLives()
    {
        int _lives = PlayerPrefs.GetInt("StartingLives", 3);
        PlayerPrefs.SetInt("lives", _lives);

        PlayerPrefs.SetInt("extra_life_step", 0);
    }

    void HideLives()
    {
        livesGameObject.SetActive(false);
        livesTextGameObject.SetActive(false);
        levelsContainer[level-1].liveRewardFiller.gameObject.SetActive(false);
    }

    public void ExtraLifeAnimation()
    {
        UpdateLives();
        uiAnimation.ExtraLifeAnimation();
    }

    public void UpdateLives()
    {
        // Get actual lives
        lives = PlayerPrefs.GetInt("lives", 3);

        // Change TextLives
        livesText.text = lives.ToString();
    }

    public void UpdateCoins()
    {
        // Get actual coins
        coins = PlayerPrefs.GetInt("coins", 0);

        // Change CoinsText
        coinsText.text = coins.ToString();
    }

    private Vector2 startedMousePos;
    private float mouseMinimumDistance = 100;
    float alpha = 1000;
    float screenCoefficient;

    void Update ()
    {
        if (Input.GetMouseButtonDown(0))
            startedMousePos = Input.mousePosition;

        // Check if ball was throwed
        if (Input.GetMouseButtonUp(0)) {
            if (!ballThrowed && ready) { 

                Vector2 mouseDistance = Input.mousePosition;
                mouseDistance -= startedMousePos;
                float shootMagnitude = mouseDistance.magnitude * screenCoefficient;

                if (shootMagnitude > mouseMinimumDistance)
                    ballThrowed = true;

            } else if (!ready)
                ready = true;
        }


        // If ball was throwed and you click --> loselife
        if (Input.GetMouseButtonDown(0) && ((ballThrowed && !levelWon && lives > 0) || tapToRespawn.activeSelf))
            LoseLife();

        CheckDeath();

        // If timer is on start counting time to reset
        if (tapTimerOnOne && !tapToRespawn.activeSelf)
        {
            //Debug.Log("TapTimerOne: " + tapTimerOne.ToString());
            tapTimerOne += Time.deltaTime;
        }

        if (tapTimerOnTwo && !tapToRespawn.activeSelf)
        {
            //Debug.Log("TapTimerTwo: " + tapTimerTwo.ToString());
            tapTimerTwo += Time.deltaTime;
        }

        // If death time has passed loss life
        if ((TAP_TIMER_TWO_DURATION < tapTimerTwo || TAP_TIMER_ONE_DURATION < tapTimerOne) && !levelWon)
        {
            tapToRespawn.SetActive(true);
        }
	}

    private float tapMinimumFromTop = 200;

    void ConfigureLevel()
    {
        LevelItem itemLevel = levelsContainer[level-1]; 

        itemLevel.gameCamera = itemLevel.level.transform.Find("MainCamera").gameObject;

        if(itemLevel.gameCamera == null)
            itemLevel.gameCamera = itemLevel.level.transform.Find("Main Camera").gameObject;

        itemLevel.mainPlayer = itemLevel.level.transform.Find("MainPlayer").gameObject;
        itemLevel.hoopFireworks = itemLevel.level.transform.Find("Hoop").gameObject.GetComponent<HoopFireworks>();
        itemLevel.liveRewardFiller = itemLevel.level.transform.Find("MainCamera/LiveRewardsComponents").gameObject.GetComponent<LiveRewardFiller>();

        levelsContainer[level-1] = itemLevel;

        // Get new main player properties
        mpRb = itemLevel.mainPlayer.GetComponent<Rigidbody>();
        mpTr = itemLevel.mainPlayer.GetComponent<Transform>();
        ballScript = itemLevel.mainPlayer.GetComponent<BallControlling> ();

        if(itemLevel.gameCamera != null)
        {
            cameraStartedPos = itemLevel.gameCamera.transform.position;
            cameraStartedRot = itemLevel.gameCamera.transform.rotation.eulerAngles;
        }

        tapToRespawn.SetActive(false);

        if (itemLevel.gameCamera != null)
        {
            itemLevel.gameCamera.transform.position = cameraStartedPos;
            itemLevel.gameCamera.transform.rotation = Quaternion.Euler(cameraStartedRot);
        }
        
        // Prepare game for next life
        // Reset ball
        ballThrowed = false;

        ready = true;
        levelWon = false;

        // Reset triggers
        triggerOne = false;
        triggerTwo = false;
        isOnFloor = false;

        if((packName == "Tutorial" && level == 3) || (packName == "TwoShootsOneLife" && level == 2))
        {
            // Hide video tutorial
            videoTutorial.SetActive(false);
        }
    }

    void LoseLife()
    {

        if(Input.mousePosition.y > Screen.height - tapMinimumFromTop)
            return;

        if (levelsContainer[level-1].gameCamera != null)
        {
            levelsContainer[level-1].gameCamera.transform.position = cameraStartedPos;
            levelsContainer[level-1].gameCamera.transform.rotation = Quaternion.Euler(cameraStartedRot);
            //Debug.Log("LoseLife()");
        }            

        if(packName != "Tutorial"){
            
            levelsContainer[level-1].liveRewardFiller.LoseLife();

            // Lose life
            lives--;
            PlayerPrefs.SetInt("lives", lives);
        }

        // Add lives lost for analytics
        livesLost++;

        // Change TextLives
        livesText.text = lives.ToString();

        // Rester Timer so LoseLife() isn't continue called 
        ResetTimerOne();
        ResetTimerTwo();

        tapToRespawn.SetActive(false);

        // Reset every door
        if(levelsContainer[level-1].buttons != null)
        {
            foreach(BotonCollider wallDoor in levelsContainer[level-1].buttons)
            {
                wallDoor.Reset();
            }
        }

        // Reset every camera trigger
        if (levelsContainer[level-1].moveCameraTriggers != null)
        {
            foreach (MoveCameraTrigger moveCamTrig in levelsContainer[level-1].moveCameraTriggers)
            {
                moveCamTrig.Restart();
            }
        }

        // Check if user lost
        if (lives <= 0 && !selected)
        {
            EndGame();

            // Don't restart level
            return;
        }
        
        // Prepare game for next life
        // Reset ball
        ballThrowed = false;

        ready = false;
        
        // Reset ball 
        ballScript.ResetBall();

        // Reset triggers
        triggerOne = false;
        triggerTwo = false;
    }

    void AddCoin()
    {
        addCoinAnimation.PlayAddCoinAnimation();
        coins += 3;
        PlayerPrefs.SetInt("coins", coins);
        UpdateCoins();
    }

    void LevelWon()
    {
        LevelItem levelItem = levelsContainer[level-1];

        if(packName != "Tutorial"){
            levelItem.liveRewardFiller.LevelWon();
        }

        AddCoin();

        //textSample.text = "Level won!";
        levelItem.hoopFireworks.PlayFireworks();
        audioManager.PlayVictorySound();
        levelWon = true;

        // Reset tap to respawn
        ResetTimerOne();
        ResetTimerTwo();
        tapToRespawn.SetActive(false);

        // UPDATE CHECKPOINT SYSTEM
        // TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO -- TODO

        // Store level won
        float max_percentageCompleted = PlayerPrefs.GetFloat(percentageCompletedCookie);
        float percentageCompleted = 100 * (float)level / numberOfLevels;
        if (max_percentageCompleted < percentageCompleted){
            PlayerPrefs.SetFloat(percentageCompletedCookie, percentageCompleted);

            if(percentageCompleted == 100f && packName == "Tutorial"){

                PlayerPrefs.SetString("PackSelected", "");
                PlayerPrefs.SetInt("FirstTimeTutorialCompleted", 1);
                
                // Trigger first time tutorial event
                AnalyticsManager.Instance.TriggerEvent("Tutorial completed for the first time.");
            }
        }

        //Debug.Log(percentageCompleted);

        // Go to next level or to menu
        Invoke("LoadNextLevel", 3f);
    }

    private void LoadNextLevel()
    {
        // Level was completed
        string eventName = packName + " level " + level.ToString() + " completed.";
        AnalyticsManager.Instance.TriggerEvent(eventName);

        // ------------- Analytics information -------------
        string skinSelected = PlayerPrefs.GetString("SkinSelected");
        string sceneName = SceneManager.GetActiveScene().name;
        
        Dictionary<string, object> dataAnalytics = new Dictionary<string, object>
        {
            {"skin_selected", skinSelected},
            {"pack", packName},
            {"level", sceneName}
        };

        AnalyticsEvent.Custom("level_completed", dataAnalytics);
        // ------------- Finish uploading analytics -------------

        if(level == numberOfLevels)
        {
            // Pack completed        

            eventName = packName + " finished.";
            AnalyticsManager.Instance.TriggerEvent(eventName);
            
            if(packName != "Tutorial")
                DisplayAdsInTime.Instance.FinishPlaying("NextScene", new Dictionary<string, string>{ {"nextScene", "MainMenu"} });
            else
                SceneManager.LoadScene("MainMenu");;
        }
        else
        {
            // Load next level
            level++;

            // Deactivate recent level
            levelsContainer[level-2].level.SetActive(false);

            // Activate new level
            levelsContainer[level-1].level.SetActive(true);

            ConfigureLevel();
        }
    }

    void EndGame()
    {
        // Increment times tried
        timesTried++;

        // Update times tried cookie
        PlayerPrefs.SetInt("timesTried_" + (level - (level % 5) + 1).ToString(), timesTried);

        // Show lost screen game
        lostScreen.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 0f);

        DisplayAdsInTime.Instance.FinishPlaying();
    }

    void CheckDeath()
    {
        // If ball wasn't throwed don't do anything 
        if (!ballThrowed || levelWon || tapToRespawn.activeSelf || lives <= 0)
            return;

        // if character height is too low, var < Y axis
        if(mpTr.position.y < -15f)
        {
            tapToRespawn.SetActive(true);
            return;
        }

        //Debug.Log("isOnFloor " + isOnFloor.ToString());
        // or it has stopped moving 
        //Debug.Log("Ball magnitude: " + mpRb.velocity.magnitude.ToString());
        if (Vector3.Distance(mpRb.velocity, new Vector3(0f, 0f, 0f)) < 3f)
        {
            tapTimerOnOne = true;
        }
        else
        {
            ResetTimerOne();
        }

        if(isOnFloor)
        {
            tapTimerOnTwo = true;
        }
        else
        {
            ResetTimerTwo();
        }
    }

    void ResetTimerOne()
    {
        //Debug.Log("ResetTimer()");
        tapTimerOnOne = false;
        tapTimerOne = 0f;
    }

    void ResetTimerTwo()
    {

        tapTimerOnTwo = false;
        tapTimerTwo = 0f;
    }

    public void TriggerOneCalled()
    {
        triggerOne = true;
        CheckWon();
    }

    public void TriggerTwoCalled()
    {
        triggerTwo = true;
        CheckWon();
    }

    void CheckWon()
    {
        if (triggerOne && triggerTwo && !levelWon)
            LevelWon();
    }

    public void EnteringFloor()
    {
        isOnFloor = true;
    }

    public void LeavingFloor()
    {
        isOnFloor = false;
    }

    public void Bouncing(Vector3 relativeVelocity)
    {
        //Debug.Log("Relative Velocity: " + relativeVelocity.magnitude.ToString());
        audioManager.PlayBounceEffect(relativeVelocity);
    }

    public void BouncingTrampoline()
    {
        // Play Trampoline Sound
        audioManager.PlayTrampolineEffect();
    }
}
