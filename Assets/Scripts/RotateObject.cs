﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour {

    public Vector3 angularSpeed;
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(angularSpeed*Time.deltaTime, Space.Self);
	}
}
