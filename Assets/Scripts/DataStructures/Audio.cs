﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Audio {

	public AudioClip audioClip;
	public float volume;

	public Audio(AudioClip _audioClip, float _volume){
		audioClip = _audioClip;
		volume = _volume;
	}

	/*public AudioClip audioClip {
		get { return itemAudioClip; }
		set { itemAudioClip = value; } 
	}

	public float volume {
		get { return itemVolume; }
		set { itemVolume = value; } 
	}*/
}
