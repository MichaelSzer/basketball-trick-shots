using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class SkinItem
{
	public string name;
	public string type;
	public int price;
	public bool ads;
	public bool purchasable;
	public string howToGetIt;
	public string imageUrl;
	public string materialUrl;
	public bool specialFunction;
	public string specialFunctionUrl;
}

[Serializable]
public class SkinData
{
	public SkinItem[] items;
}