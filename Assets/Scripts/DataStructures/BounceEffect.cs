﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct BounceEffect {

	public Audio audio;
	public float threshold;

	public BounceEffect(Audio _audio, float _threshold){
		audio = _audio;
		threshold = _threshold;
	}
}
