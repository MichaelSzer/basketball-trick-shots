using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

[RequireComponent(typeof(HandlePurchase))]
// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class IAPManager : MonoBehaviour, IStoreListener
{
    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.
    
    public static string coins500ID = "coins500";    
    public static string coins3000ID = "coins3000";
    public static string coins7500ID = "coins7500";
    public static string adsRemoverID = "remove_ads";

    // Apple App Store-specific product identifier for the subscription product.
    //private static string kProductNameAppleSubscription =  "com.unity3d.subscription.new";
    
    // Google Play Store-specific product identifier subscription product.
    //private static string kProductNameGooglePlaySubscription =  "com.unity3d.subscription.original"; 

    public static IAPManager Instance;
    private HandlePurchase handlePurchase;
    private Text coinsText;

    void Awake(){
    	if(CheckIfDuplicate())
    		Destroy(gameObject);
    }

    void Start()
    {
    	DontDestroyOnLoad(gameObject);
    	Instance = this;

        // If we haven't set up the Unity Purchasing reference
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }

        handlePurchase = GetComponent<HandlePurchase> ();
    }

    bool CheckIfDuplicate(){

		if(IAPManager.Instance != null)
			return true;

		return false;
	}
    
    public void InitializePurchasing() 
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }
        
        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        builder.AddProduct(coins500ID, ProductType.Consumable);
        builder.AddProduct(coins3000ID, ProductType.Consumable);
        builder.AddProduct(coins7500ID, ProductType.Consumable);
        builder.AddProduct(adsRemoverID, ProductType.NonConsumable);
        
        UnityPurchasing.Initialize(this, builder);
    }
    
    
    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void SetCoinsText(Text text)
    {
    	coinsText = text;
    }  

    void UpdateCoinsText(){	

    	int coins = PlayerPrefs.GetInt("coins", 0);
    	coinsText.text = coins.ToString();

    	if(coinsText == null)
    	{
    		Debug.LogError("UpdateCoinsText() wasn't called.");
    		return;
    	}
    }  
    
    public void Buy500Coins()
    {
        BuyProductID(coins500ID);
    }
    
    public void Buy3000Coins()
    {
        BuyProductID(coins3000ID);
    }

    public void Buy7500Coins()
    {
        BuyProductID(coins7500ID);
    }

    public void BuyAdsRemover()
    {
        BuyProductID(adsRemoverID);
    }
    
    private void BuyProductID(string productId)
    {
        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = m_StoreController.products.WithID(productId);
            
            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                m_StoreController.InitiatePurchase(product);
            }
            // Otherwise ...
            else
            {
                // ... report the product look-up failure situation  
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        // Otherwise ...
        else
        {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }
    
    
    // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
    // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
    public void RestorePurchases()
    {
        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }
        
        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer || 
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");
            
            // Fetch the Apple store-specific subsystem.
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) => {
                // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                // no purchases are available to be restored.
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }
    
    
    //  
    // --- IStoreListener
    //
    
    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        Debug.Log("OnInitialized: PASS");
        
        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;
    }
    
    
    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }
    
    
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
    {
        // A consumable product has been purchased by this user.
        if (String.Equals(args.purchasedProduct.definition.id, coins500ID, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            
            handlePurchase.HandleBuy500Coins();
            UpdateCoinsText();
        }
        // Or ... an unknown product has been purchased by this user. Fill in additional products here....
        else if (String.Equals(args.purchasedProduct.definition.id, coins3000ID, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            
            handlePurchase.HandleBuy3000Coins();
            UpdateCoinsText();
        }
        else if (String.Equals(args.purchasedProduct.definition.id, coins7500ID, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            
            handlePurchase.HandleBuy7500Coins();
            UpdateCoinsText();
            
        }
        else if (String.Equals(args.purchasedProduct.definition.id, adsRemoverID, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            
            handlePurchase.HandleBuyAdsRemover();
        }
        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        }

        // Return a flag indicating whether this product has completely been received, or if the application needs 
        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
        // saving purchased products to the cloud, and when that save is delayed. 
        return PurchaseProcessingResult.Complete;
    }
    
    
    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}