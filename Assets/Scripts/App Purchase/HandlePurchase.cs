using System;
using System.Collections.Generic;
using UnityEngine;

public class HandlePurchase : MonoBehaviour
{

	void AddCoins(int quantity){

		int coins = PlayerPrefs.GetInt("coins", 0);
		coins += quantity;
		PlayerPrefs.SetInt("coins", coins);
	}

	public void HandleBuy500Coins(){
		HandleBuyAdsRemover();
		AddCoins(500);
	}

	public void HandleBuy3000Coins(){
		HandleBuyAdsRemover();
		AddCoins(3000);
	}

	public void HandleBuy7500Coins(){
		HandleBuyAdsRemover();
		AddCoins(7500);
	}

	public void HandleBuyAdsRemover(){
	
		PlayerPrefs.SetInt("adsRemover", 1);	
	}
}