using System;

public class SkinOwn
{
	public string skin;
	public int year;
	public int month;
	public int day;
	public int minute;
	public int second;

	public SkinOwn(string name)
	{
		skin = name;

		DateTime nowTime = DateTime.Now;
		year = nowTime.Year;
		month = nowTime.Month;
		day = nowTime.Day;
		minute = nowTime.Minute;
		second = nowTime.Second;
	}
}