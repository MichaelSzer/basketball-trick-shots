﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SkinsPage : MonoBehaviour {

	public List<GameObject> listSkinsGameObject;

	public List<GameObject> GetListGameObjects(int num)
	{
		return listSkinsGameObject.GetRange(0, num);
	}
}
