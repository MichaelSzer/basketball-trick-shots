using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class SkinsInventoryManager : MonoBehaviour {

	public InventoryPagesManager inventoryPagesManager;
	public GameObject pageTemplate;
	public GameObject parentPages;

	private int skinsPerPage = 18;

	private List<GameObject> pages;

	bool setUp = false;

	public List<GameObject> GetSkinsGameObjects(int numberOfSkins)
	{
		if(setUp)
		{
			Debug.LogError("This inventory was already set up.");
			return null;
		}

		setUp = true;

		List<GameObject> listSkinsGameObject = new List<GameObject> ();
		pages = new List<GameObject> ();

		int numberOfPages = (numberOfSkins / skinsPerPage) + 1; 

		if(numberOfSkins % skinsPerPage == 0 && numberOfPages > 1)
			numberOfPages--;

		for(int i = 0; i < numberOfPages; i++)
		{
			GameObject page = Instantiate(pageTemplate, parentPages.transform);

			pages.Add(page);

			int res = skinsPerPage;

			if(numberOfSkins < skinsPerPage)
				res = numberOfSkins;

			numberOfSkins -= res;

			listSkinsGameObject = listSkinsGameObject.Concat(page.GetComponent<SkinsPage> ().GetListGameObjects(res)).ToList();
		}

		inventoryPagesManager.SetupSkinsPages(pages);

		return listSkinsGameObject;
	}
}
