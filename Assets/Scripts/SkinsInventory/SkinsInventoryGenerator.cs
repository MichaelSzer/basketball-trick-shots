﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SkinsInventoryGenerator : MonoBehaviour {

	public GameObject inventoryAll;
	public GameObject inventoryCommon;
	public GameObject inventoryRare;
	public GameObject inventoryEpic;
	public GameObject inventoryLegendary;

	// skinsInvetoryManager = sim
	private SkinsInventoryManager simAll;
	private SkinsInventoryManager simCommon;
	private SkinsInventoryManager simRare;
	private SkinsInventoryManager simEpic;
	private SkinsInventoryManager simLegendary;

	private List<string> allSkins;
	private List<string> commonSkins;
	private List<string> rareSkins;
	private List<string> epicSkins;
	private List<string> legendarySkins;

	void Start()
	{
		InitializeLists();
		GetSkinsArray();
		CreateEveryInventoryType();
	}

	void GetSkinsArray()
	{
		string[] names = PlayerPrefsX.GetStringArray("MySkins", "", 0);
		
		foreach(string name in names)
		{
			allSkins.Add(name);

			string type = DataManager.Instance.skinsDictionary[name].type.ToLower();

			if(type == "common")
				commonSkins.Add(name);
			else if(type == "rare")
				rareSkins.Add(name);
			else if(type == "epic")
				epicSkins.Add(name);
			else if(type == "legendary")
				legendarySkins.Add(name);
		}

	}

	void InitializeLists()
	{
		allSkins = new List<string> ();
		commonSkins = new List<string> ();
		rareSkins = new List<string> ();
		epicSkins = new List<string> ();
		legendarySkins = new List<string> ();
	}

	void CreateEveryInventoryType()
	{
		CreateSkinsInvetory(inventoryAll, allSkins);
		CreateSkinsInvetory(inventoryCommon, commonSkins);
		CreateSkinsInvetory(inventoryRare, rareSkins);
		CreateSkinsInvetory(inventoryEpic, epicSkins);
		CreateSkinsInvetory(inventoryLegendary, legendarySkins);
	}

	void CreateSkinsInvetory(GameObject inventory, List<string> skinList)
	{
		SkinsInventoryManager sim = inventory.GetComponent<SkinsInventoryManager> ();

		List<GameObject> listGameObjects = sim.GetSkinsGameObjects(skinList.Count);

		for (int i = 0; i < listGameObjects.Count; i++)
		{
			SkinItem item = DataManager.Instance.skinsDictionary[skinList[i]];
			ConfigureSkin(listGameObjects[i], item);
		}

		// Lack of the other inventories configuration...
	}

	void ConfigureSkin(GameObject skinGameObject, SkinItem item)
	{
		//Debug.Log(item.imageUrl);

		//Material material = Resources.Load<Material>("Materials/" + item.materialUrl);
		Sprite image = Resources.Load<Sprite>(item.imageUrl);

		//skinGameObject.GetComponent<Renderer> ().material = material;
		skinGameObject.GetComponent<Image> ().sprite = image;
		skinGameObject.GetComponent<Button> ().onClick.AddListener(() => ShowInPreviewBall(item.name));

		skinGameObject.SetActive(true);
	}

	void ShowInPreviewBall(string skinName)
	{
		Debug.Log("Showing skin: " + skinName);
		SkinsInventoryBallPreview.Instance.ShowSkin(skinName);
	}
}
