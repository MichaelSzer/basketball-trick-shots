﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinsInventoryBallPreview : MonoBehaviour {

	public static SkinsInventoryBallPreview Instance;

	public GameObject buttonSelect;  
	private string skin;
	public GameObject ball;
	public GameObject frameCommon, frameRare, frameEpic, frameLegendary;
	public GameObject inventory, container;

	void Awake()
	{
		if(Instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			Instance = this;
		}
	}

	void HideFrames()
	{
		frameCommon.SetActive(false);
		frameRare.SetActive(false);
		frameEpic.SetActive(false);
		frameLegendary.SetActive(false);
	}

	public void ShowSkin(string _skin)
	{
		inventory.SetActive(false);
		container.SetActive(true);

		skin = _skin;
		SkinItem skinItem = DataManager.Instance.skinsDictionary[skin];

		HideFrames();

		switch (skinItem.type.ToLower())
		{
			case "common":
				frameCommon.SetActive(true);
				break;
			case "rare":
				frameRare.SetActive(true);
				break;
			case "epic":
				frameEpic.SetActive(true);
				break;
			case "legendary":
				frameLegendary.SetActive(true);
				break;
		}

		ball.GetComponent<Renderer> ().material = Resources.Load<Material> (skinItem.materialUrl);

		string currentSkin = PlayerPrefs.GetString("SkinSelected", "");

		if(currentSkin == skin)
		{
			buttonSelect.SetActive(false);
		}
		else
		{
			buttonSelect.SetActive(true);	
		}
	}

	public void SelectSkin()
	{
		buttonSelect.SetActive(false);
		PlayerPrefs.SetString("SkinSelected", skin);
	}

	public void ReturnToInventory()
	{
		inventory.SetActive(true);
		container.SetActive(false);
	}
}
