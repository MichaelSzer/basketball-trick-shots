﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowInventory : MonoBehaviour {

	public GameObject inventoryAll;
	public GameObject inventoryCommon;
	public GameObject inventoryRare;
	public GameObject inventoryEpic;
	public GameObject inventoryLegendary;

	void HideAll()
	{
		inventoryAll.SetActive(false);
		inventoryCommon.SetActive(false);
		inventoryRare.SetActive(false);
		inventoryEpic.SetActive(false);
		inventoryLegendary.SetActive(false);
	}

	public void ShowInventoryAll()
	{
		if(inventoryAll.activeSelf)
			return;

		HideAll();
		inventoryAll.SetActive(true);
	}

	public void ShowInventoryCommon()
	{
		if(inventoryCommon.activeSelf)
			return;

		HideAll();
		inventoryCommon.SetActive(true);
	}

	public void ShowInventoryRare()
	{
		if(inventoryRare.activeSelf)
			return;

		HideAll();
		inventoryRare.SetActive(true);
	}

	public void ShowInventoryEpic()
	{
		if(inventoryEpic.activeSelf)
			return;

		HideAll();
		inventoryEpic.SetActive(true);
	}

	public void ShowInventoryLegendary()
	{
		if(inventoryLegendary.activeSelf)
			return;

		HideAll();
		inventoryLegendary.SetActive(true);
	}
}
