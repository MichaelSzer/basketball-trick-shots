using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryPagesManager : MonoBehaviour {

    private List<GameObject> skinsPages;
    public Text pagesText;

    private int pages = -1, actualPage = 1;
    private GameObject showingPage;

    public void SetupSkinsPages(List<GameObject> _skinsPages)
    {
        skinsPages = _skinsPages;
        pages = skinsPages.Count;
        UpdatePagesText(actualPage);
        ShowPage(actualPage);
    }

    bool CheckSetup()
    {
        if(pages == -1)
        {
            Debug.LogError("SetupSkinsPages() must be called before using this script.");
            return false;
        }
        else
            return true;
    }

    void ShowPage(int page)
    {
        if(!CheckSetup())
            return;

        if (page < 1 || page > pages)
        {
            Debug.LogError("La pagina " + page.ToString() + " pedida no existe.");
            return;
        }

        if(showingPage != null)
            showingPage.SetActive(false);

        showingPage = skinsPages[page - 1];
        showingPage.SetActive(true);

        PlayerPrefs.SetInt("SkinPage", page);

        UpdatePagesText(page);
    }

    public void NextPage()
    {
        if(!CheckSetup())
            return;

        actualPage++;

        if (actualPage > pages)
            actualPage = 1;

        ShowPage(actualPage);
    }

    public void PrevoiusPage()
    {
        if(!CheckSetup())
            return;

        actualPage--;

        if (actualPage < 1)
            actualPage = pages;

        ShowPage(actualPage);
    }

    void UpdatePagesText(int page)
    {
        if(!CheckSetup())
            return;

        pagesText.text = page.ToString() + " / " + pages.ToString();
    }
}
