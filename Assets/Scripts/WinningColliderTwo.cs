﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinningColliderTwo : MonoBehaviour {

    private GameObject brainGameObject;
    private Brain brain;

    private void Awake()
    {
        // Get Brain Game Object
        brainGameObject = GameObject.Find("Brain");

        // Get Brain script
        brain = brainGameObject.GetComponent<Brain>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "MainPlayer")
            brain.TriggerTwoCalled();
	}
}
