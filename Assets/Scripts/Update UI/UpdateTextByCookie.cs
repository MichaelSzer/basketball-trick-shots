﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateTextByCookie : MonoBehaviour {

	public string cookie = "";
	Text text;
	TextMesh textMesh;

	void Awake()
	{
		text = GetComponent<Text> ();

		if(text == null)
			textMesh = GetComponent<TextMesh> ();

		if(text == null && textMesh == null)
			Debug.LogError("UpdateTextCoins script is only for 'Text' or 'TextMesh'");

		if(cookie == "")
			Debug.LogError("Cookie must be set");		
	}

	void OnEnable()
	{
		int value = PlayerPrefs.GetInt(cookie, 0);

		if(text != null)
			text.text = value.ToString();
		else if(textMesh != null)
			textMesh.text = value.ToString();
	}
}
