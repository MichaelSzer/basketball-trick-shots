﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateBallPreviewFrame : MonoBehaviour {

	public GameObject common, rare, epic, legendary;

	void Awake()
	{
		if(common == null || rare == null || epic == null || legendary == null)
			Debug.LogError("The ball preview frame was not set correctly.");
	}

	void HideAll()
	{
		common.SetActive(false);
		rare.SetActive(false);
		epic.SetActive(false);
		legendary.SetActive(false);
	}

	void OnEnable()
	{
		string skin = PlayerPrefs.GetString("SkinSelected", "SkinDefault");

		SkinItem item = DataManager.Instance.GetSkinDictionary(skin);

		switch(item.type.ToLower())
		{
			case "common":
				HideAll();
				common.SetActive(true);
				break;
			case "rare":
				HideAll();
				rare.SetActive(true);
				break;
			case "epic":
				HideAll();
				epic.SetActive(true);
				break;
			case "legendary":
				HideAll();
				legendary.SetActive(true);
				break;
			default:
				HideAll();
				common.SetActive(true);
				break;
		}
	}
}
