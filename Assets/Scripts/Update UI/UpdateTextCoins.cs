﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateTextCoins : MonoBehaviour {

	Text textCoins;

	void Awake()
	{
		textCoins = GetComponent<Text> ();

		if(textCoins == null)
			Debug.LogError("UpdateTextCoins script is only for 'Text'");
	}

	void OnEnable()
	{
		int coins = PlayerPrefs.GetInt("coins", 0);
		textCoins.text = coins.ToString();
	}
}
