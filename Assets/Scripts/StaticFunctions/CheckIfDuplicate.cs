using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CheckIfDuplicate
{
	public static bool function(string name)
	{
		GameObject[] dontDestroyOnLoadObjects = GetDontDestroyOnLoadObjects.function();
		//Debug.Log("DontDestroyOnLoad GameObjects: " + dontDestroyOnLoadObjects.Length.ToString());

		foreach(GameObject gb in dontDestroyOnLoadObjects){
			if(gb.name == name)
				return true;
		}

		return false;
	}
	
}
