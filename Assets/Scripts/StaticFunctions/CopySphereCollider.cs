﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CopySphereCollider {

	public static SphereCollider function(SphereCollider original, GameObject destination)
 	{
	    SphereCollider copy = destination.AddComponent<SphereCollider> ();
	    
	    copy.center = original.center;
	    copy.radius = original.radius;
	    copy.material = original.material;
	    copy.isTrigger = original.isTrigger;

	    return copy;
 	}
}
