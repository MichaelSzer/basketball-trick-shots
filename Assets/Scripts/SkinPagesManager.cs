﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinPagesManager : MonoBehaviour {

    public List<GameObject> skinsPages;
    public Text pagesText;

    private int pages, actualPage;
    private GameObject showingPage;

	void Start () {
        pages = skinsPages.Count;

        actualPage = PlayerPrefs.GetInt("SkinPage", 1);
        ShowPage(actualPage);
	}

    void ShowPage(int page)
    {
        if (page < 1 || page > pages)
        {
            Debug.LogError("La pagina " + page.ToString() + " pedida no existe.");
            return;
        }

        if(showingPage != null)
            showingPage.SetActive(false);

        showingPage = skinsPages[page - 1];
        showingPage.SetActive(true);

        PlayerPrefs.SetInt("SkinPage", page);

        UpdatePagesText(page);
    }

    public void NextPage()
    {
        actualPage++;

        if (actualPage > pages)
            actualPage = 1;

        ShowPage(actualPage);
    }

    public void PrevoiusPage()
    {
        actualPage--;

        if (actualPage < 1)
            actualPage = pages;

        ShowPage(actualPage);
    }

    void UpdatePagesText(int page)
    {
        pagesText.text = page.ToString() + " / " + pages.ToString();
    }
}
