﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyMoreCoinsShop : MonoBehaviour {

    public Text coinsText;

    private void OnEnable()
    {
        UpdateCoins();
    }

    void UpdateCoins()
    {
        int coins = PlayerPrefs.GetInt("coins", 0);
        coinsText.text = coins.ToString();
    }
}
