using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour {

	public static PauseManager Instance;

	void Awake(){ Instance = this; }

	void Start(){ DontDestroyOnLoad(gameObject); }

	public void Pause()
	{
		Time.timeScale = 0f;
		SoundtrackManager.Instance.PauseSoundtrack();
	}

	public void Resume()
	{
		Time.timeScale = 1f;
		SoundtrackManager.Instance.ResumeSoundtrack();
	}
}
