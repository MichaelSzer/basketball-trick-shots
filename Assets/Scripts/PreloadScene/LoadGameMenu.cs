﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadGameMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GoToMenuScene();	
	}

	void GoToMenuScene()
	{
		SceneManager.LoadScene("MainMenu");
	}
}
