using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DataManager : MonoBehaviour {

	public static DataManager Instance;

	private string skinsDataFileName = "Skins";
	public Dictionary<string, SkinItem> skinsDictionary;

	private bool ready = false;

	void Awake()
	{
		if(Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}

	void Start()
	{
		LoadSkinsData();
		//PrintDataDictionary();
	}

	void LoadSkinsData()
	{
		skinsDictionary = new Dictionary<string, SkinItem> ();

		string filePath = "SetupData/" + skinsDataFileName;

		TextAsset targetFile = Resources.Load<TextAsset>(filePath);

		if(targetFile == null)
		{
			Debug.LogError("Skins Data not found in Assets/SteramingAssets/ " + skinsDataFileName);
			return;
		}

		string dataAsJson = targetFile.text;

		SkinData skinData = JsonUtility.FromJson<SkinData> (dataAsJson);

		foreach(SkinItem item in skinData.items)
		{
			skinsDictionary.Add(item.name, item);
		}

		ready = true;

		/*string filePath;

		switch(Application.platform)
		{
			case RuntimePlatform.Android:
				filePath = "jar:file://" + Application.dataPath + "!/assets/" + skinsDataFileName;
				break;
			case RuntimePlatform.IPhonePlayer:
				filePath = Application.dataPath + "/Raw" + skinsDataFileName;
				break;
			default:
				filePath = Application.streamingAssetsPath + skinsDataFileName;
				break;
		}

		if(File.Exists(filePath))
		{
			string dataAsJson;

			if (Application.platform == RuntimePlatform.Android)
			{
				WWW reader = new WWW(filePath);
				while (!reader.isDone) {}

				dataAsJson = reader.text;
			}
			else
			{
				dataAsJson = File.ReadAllText(filePath);
			}

			SkinData skinData = JsonUtility.FromJson<SkinData> (dataAsJson);

			foreach(SkinItem item in skinData.items)
			{
				skinsDictionary.Add(item.name, item);
			}

			ready = true;
		}
		else
		{
			Debug.LogError("Skins Data not found in Assets/SteramingAssets/ " + skinsDataFileName);
		}*/
	}

	public bool IsReady()
	{
		return ready;
	}

	public void PrintDataDictionary()
	{
		foreach (KeyValuePair<string, SkinItem> item in skinsDictionary)
		{
			Debug.Log(string.Format("Key = {0}", item.Key));
		}
	}

	public SkinItem GetSkinDictionary(string skin)
	{
		return skinsDictionary[skin];
	}
}
