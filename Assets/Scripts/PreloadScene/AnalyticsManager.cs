﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

[RequireComponent(typeof(AnalyticsTracker))]
public class AnalyticsManager : MonoBehaviour {

	public static AnalyticsManager Instance;

	private AnalyticsTracker analyticsTracker;

	void Awake()
	{
		if(CheckIfDuplicate.function("AnalyticsManager"))
		{
			Debug.LogError("There are two AnalyticsManager, something is wrong. S.O.S.");
			Destroy(gameObject);
		}

		DontDestroyOnLoad(gameObject);
		Instance = this;
	}

	// Use this for initialization
	void Start () {
		
		// Check requirments
		analyticsTracker = GetComponent<AnalyticsTracker> ();

		if(analyticsTracker == null)
		{
			Debug.LogError("AnalyticsManager must have attached 'AnalyticsTracker'");
		}
	}
	
	public void TriggerEvent(string eventName)
	{
		analyticsTracker.eventName = eventName;
		analyticsTracker.TriggerEvent();

		//Debug.Log("TriggerEvent( " + eventName + " )");
	}
}
