﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteTunningVariables : MonoBehaviour {

	public float DefaultTimeToDisplayAds = 180f;
	public float TimeToDisplayAds{ get; private set; }

	public float DefaultCoinsMultiplier = 1f;
	public float CoinsMultiplier{ get; private set; }

	public float DefaultArcadeCoinsPerPoint = 0.5f;
	public float ArcadeCoinsPerPoint{ get; private set; }

	public bool Ready{ get; private set; }

	// Singleton pattern
	private static RemoteTunningVariables _instance;
	public static RemoteTunningVariables Instance
	{
		get { return _instance; }
	}

	void Awake()
	{
		if(_instance != null && Instance != this)
		{
			Destroy(gameObject);
			return;
		}


		_instance = this;
		DontDestroyOnLoad(gameObject);

		Ready = false;
		
		LoadDefaultValues();
		LoadRemoteSetting();

		RemoteSettings.Updated += RemoteSettingUpdated;
	}

	void LoadDefaultValues()
	{
		TimeToDisplayAds = DefaultTimeToDisplayAds;
		CoinsMultiplier = DefaultCoinsMultiplier;
		ArcadeCoinsPerPoint = DefaultArcadeCoinsPerPoint;
	}

	// Variables that use values stored in cache
	void LoadRemoteSetting()
	{
		TimeToDisplayAds = RemoteSettings.GetFloat("TimeToDisplayAds", DefaultTimeToDisplayAds);
		ArcadeCoinsPerPoint = RemoteSettings.GetFloat("ArcadeCoinsPerPoint", DefaultArcadeCoinsPerPoint);
	}

	// Variables that dont use values stored in cache
	void RemoteSettingUpdated()
	{
		CoinsMultiplier = RemoteSettings.GetFloat("CoinsMultiplier", DefaultCoinsMultiplier);
		//Debug.Log("CoinsMultiplier " + CoinsMultiplier.ToString());

		Ready = true;
	}
}