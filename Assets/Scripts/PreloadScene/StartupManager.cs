﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartupManager : MonoBehaviour {

	private float animationTime = 2f;

	// Use this for initialization
	private IEnumerator Start () {
		
		while(!DataManager.Instance.IsReady())
		{
			yield return null;
		}

		float timer = 0f;

		while(timer < animationTime)
		{
			timer += Time.deltaTime;
			yield return null;
		}

		if(!RemoteTunningVariables.Instance.Ready)
		{
			Debug.Log("RemoteTunningVariables did not fetched online values.");
		}

		PlayerPrefs.SetString("PackSelected", "");

		GoToMenuScene();
	}

	void GoToMenuScene()
	{
		SceneManager.LoadScene("MainMenu");
	}
}
