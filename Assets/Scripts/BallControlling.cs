﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]
public class BallControlling : MonoBehaviour {

    protected LineRenderer lr;
    protected Rigidbody rb;
    protected Transform tr;
    private float gravity;
    private bool tapped = false;
    private List<Vector3> linePositions;
    const int NUM_SEGMENTS = 32;
    const float BALL_SPEED_FORCE_ONE = 0.035f;
    const float BALL_SPEED_FORCE_TWO = 0.028f;
    const float BALL_ANGULAR_SPEED_FORCE = 0.5f;
    private Vector2 BALL_START_DISTANCE;
    private Vector2 pivotPoint;
    private Vector3 ballVelocity;
    private Vector3 ballAngularVelocity;
    private Vector3 startPosition;
    private GameObject brainGameObject;
    private Brain brain;
    protected bool activated = true;
    private bool ready = true;
    private int scopeAccuracy;
    public bool freezePosition2D = false;
    private float lrWidthStart = 0.20f;
    private float lrWidthFinal = 0.15f;
    private List<IOnCollision> objectsWithCollision;

    void Awake()
    {
        lr = GetComponent<LineRenderer> ();
        rb = GetComponent<Rigidbody> ();
        tr = GetComponent<Transform> ();

        // Get brain gameobject
        brainGameObject = GameObject.Find("Brain");

        // Get script from gameobject
        brain = brainGameObject.GetComponent<Brain> ();

        InitializeValues();
    }

    void Start()
    {
        // Get objectsWithCollision after they are created in SkinSelect.Awake() 
        objectsWithCollision = new List<IOnCollision> ();

        foreach (Transform child in transform){
            IOnCollision objectWithCollision = child.GetComponent<IOnCollision> ();

            if(objectWithCollision != null)
                objectsWithCollision.Add(objectWithCollision);
        }
    }

    protected void InitializeValues ()
    {
        // Configure linerenderer
        lr.startWidth = lrWidthStart;
        lr.endWidth = lrWidthFinal;

        // Get gravity
        gravity = Mathf.Abs(Physics.gravity.y);

        // Get start position
        startPosition = transform.position;

        // Set starting distance from pivot
        BALL_START_DISTANCE = new Vector2(25f, 25f);

        // Get scope accuracy
        scopeAccuracy = PlayerPrefs.GetInt("ScopeAccuracy", 50);

        screenCoefficient = alpha / Mathf.Sqrt(Screen.width * Screen.height);

        linePositions = new List<Vector3> ();
    }

    Vector3 GetPositionInTime(float time, float velZ, float VelY)
    {
        float posY, posZ;

        // Calculate postion in Z axis
        posZ = time * velZ;

        // Calculate position in Y axis
        posY = time * VelY - 0.5f * gravity * time * time;

        return new Vector3(0f, posY, posZ);
    }

    void SetupLine(Vector3 velocity)
    {
        // Transform angle from Degree to Radians
        //float angRad = angDeg * Mathf.Deg2Rad;

        float velY = velocity.y; float velZ = velocity.z;

        // Store actual velocity in a Vector
        ballVelocity = new Vector3(0f, velY, velZ);

        if(velZ < 0f)
            ballAngularVelocity = new Vector3(velocity.magnitude * -BALL_ANGULAR_SPEED_FORCE, 0f, 0f);
        else
            ballAngularVelocity = new Vector3(velocity.magnitude * BALL_ANGULAR_SPEED_FORCE, 0f, 0f);

        //Debug.Log(ballVelocity);

        // Calculate total time flight
        float timeFlying = Mathf.Abs( (2 * velY) / gravity ) * ((float)scopeAccuracy / 100);

        // Time between segments
        float timeBetweenSegments = timeFlying / NUM_SEGMENTS;

        // Calculate Vector Postions
        linePositions.Clear();

        for(int i = 0; i < NUM_SEGMENTS; i++)
        {
            float time = timeFlying - (timeBetweenSegments * (NUM_SEGMENTS - i));
            linePositions.Add(transform.position + GetPositionInTime(time, velZ, velY));
        }

        RenderLine(linePositions);
    }

    void RenderLine(List<Vector3> positions)
    {
        // Set line renderer segments
        lr.positionCount = NUM_SEGMENTS;

        for(int i = 0; i < positions.Count; i++)
        {
        	// Set line renderer positions
        	lr.SetPosition(i, positions[i]);
        }
    }

    private Vector2 startedMousePos;
    private float mouseMinimumDistance = 100;

    protected void Update()
    {
        //Debug.Log(Input.mousePosition);

        // Check if the ball is activated
        if (!activated)
            return;

        // Check if user is touching the screen
        if (Input.GetMouseButton(0) && !tapped && ready)
        {
            startedMousePos = Input.mousePosition;

            // If the user just tap --> make a pivot point there
            tapped = true;
            /*Vector3 tempPoint = Input.mousePosition;
            tempPoint.z = 1;
            tempPoint = Camera.main.ScreenToWorldPoint(tempPoint);
            pivotPoint = new Vector2(tempPoint.z, tempPoint.y);*/

            pivotPoint = startedMousePos;

            // Show line renderer
            lr.enabled = true;
        }

        // Check if the user realased the tap
        if (Input.GetMouseButtonUp(0))
        {
            Vector2 mouseDistance = Input.mousePosition;
            mouseDistance -= startedMousePos;
            float shootMagnitude = mouseDistance.magnitude * screenCoefficient;

            if (ready && shootMagnitude > mouseMinimumDistance)
            {
                ThrowBall();
            }
            else
            {
                ready = true;
            }

            tapped = false;
        }

        // Check if user continue touching the screen
        if (tapped)
        {
            GetDistanceToPivot();
        }
    }

    float alpha = 1000;
    float screenCoefficient;

    void GetDistanceToPivot()
    {
        // Store current tap position
        /*Vector3 tempPoint = Input.mousePosition;
        tempPoint.z = 1;
        tempPoint = Camera.main.ScreenToWorldPoint(tempPoint);
        Vector2 tapPos = new Vector2(tempPoint.z, tempPoint.y);*/

        Vector2 tapPos = Input.mousePosition;

        // Get distance from pivot point
        //Vector2 distanceFromPivot = pivotPoint - tapPos + BALL_START_DISTANCE;
        Vector2 distanceFromPivot = pivotPoint - tapPos;

        float BALL_SPEED_FORCE;

        // Check how the user is tapping and adjust the force to the right one
        if(distanceFromPivot.y < 0){
            distanceFromPivot = distanceFromPivot * -1;
            BALL_SPEED_FORCE = BALL_SPEED_FORCE_TWO;
        }
        else
        {
            BALL_SPEED_FORCE = BALL_SPEED_FORCE_ONE;   
        }

        // Get Vector Magnitude
        float magnitude = Mathf.Sqrt((distanceFromPivot.x * distanceFromPivot.x) + (distanceFromPivot.y * distanceFromPivot.y));
                //Debug.Log(Input.mousePosition);       
        

        // Transform magnitude into ball velocity
        float ballSpeed = magnitude * BALL_SPEED_FORCE * screenCoefficient;

        // Get angle of trajectory
        float angleInRad = Mathf.Atan2(distanceFromPivot.y, distanceFromPivot.x);

        // Calculate ball velocities in each axis
        ballVelocity = CalculateBallVelocity(ballSpeed, angleInRad);

        // Setup Line with new tap position
        SetupLine(ballVelocity);
    }

    Vector3 CalculateBallVelocity(float vl, float angRad)
    {
        // Calculate axis z and y velocity
        float velZ, velY;
        velZ = vl * Mathf.Cos(angRad); velY = vl * Mathf.Sin(angRad);

        return new Vector3(0f, velY, velZ);
    }

    protected void ThrowBall()
    {
        // Make ball only move in 2D
        if(freezePosition2D)
            rb.constraints = RigidbodyConstraints.FreezePositionX;

        // Hide line renderer
        lr.enabled = false;

        rb.isKinematic = false;

        // Give ball velocity
        rb.velocity = ballVelocity;

        // Give ball angular velocity
        rb.angularVelocity = ballAngularVelocity;

        // Deactivate ball
        activated = false;
    }

    public void ResetBall()
    {
        // return ball to start point and setup properties
        tr.position = startPosition;
        tr.rotation = Quaternion.identity;
        rb.velocity = new Vector3(0f, 0f, 0f);
        rb.angularVelocity = Vector3.zero;
        ready = true;
        ActivateBall();
    }

    public void AnotherShoot()
    {
        // Configure ball
        rb.velocity = new Vector3(0f, 0f, 0f);
        rb.angularVelocity = Vector3.zero;
        rb.isKinematic = true;
        ActivateBall();

        // Configure brain
        brain.ballThrowed = false;
    }

    public void ActivateBall()
    {
        activated = true;
    }

    void OnCollisionEnter(Collision collision)
    {
        for (int i = 0; i < objectsWithCollision.Count; i++){
            objectsWithCollision[i].OnCollisionEnter(collision);
        }

        if(collision.gameObject.tag == "Floor")
        {
            brain.EnteringFloor();
        }    

        // If the ball bounce, play sound effects 
        brain.Bouncing(collision.relativeVelocity);
    }

    void OnCollisionStay(Collision collision)
    {
        for (int i = 0; i < objectsWithCollision.Count; i++){
            objectsWithCollision[i].OnCollisionStay(collision);
        }
    }

    void OnCollisionExit(Collision collision)
    {
        for (int i = 0; i < objectsWithCollision.Count; i++){
            objectsWithCollision[i].OnCollisionExit(collision);
        }

        if (collision.gameObject.tag == "Floor")
        {
            brain.LeavingFloor();
        }
    }
}
