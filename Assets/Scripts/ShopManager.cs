﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

//#if UNITY_ADS // only compile Ads code on supported platforms
using UnityEngine.Monetization;
//#endif

public class ShopManager : MonoBehaviour {
    
    public GameObject mainMenu, shop, buySkins, ballPreview, buyMoreCoins, ball;

    // ------------------- Life Upgrade -------------------

    public const int defaultStartingLives = 3;

    public Text upgradeLifeText, priceLifeText;
    private int actualStartingLifes;
    private int maxUpgradeLifes;
    public List<int> livesPrice;

    // ------------------- Scope Upgrade -------------------

    public Text upgradeScopeText, priceScopeText;
    private int actualScopeAccuracy, indexValScope;
    public List<int> scopePrice, scopeUpgrade;

    // ------------------- Coins -------------------

    public Text buyMoreCoinsText;
    public Text coinsText;
    private int coins;

    // ------------------- Skins -------------------

    private string skinSelected;
    private string actualSkin;

    public GameObject previewBall;
    public Text ballPrice;
    public GameObject aquiredInterface;
    public GameObject buyInterface;
    public GameObject selectInterface;
    public GameObject adInterface;
    public Text adsLeft;
    public GameObject specialInterface;
    public Text howToGetIt;

    public GameObject common, rare, epic, legendary;

    private SkinItem skin;

    // ------------------- Audio -------------------

    public AudioSource audioSource;
    public AudioClip invalidPurchase;
    public AudioClip successfulPurchase;

    // Use this for initialization
    void OnEnable ()
    {
        ballPreview.SetActive(true);

        skinSelected = PlayerPrefs.GetString("SkinSelected", "SkinDefault");
        actualSkin = skinSelected;

        UpdateSkin();

        actualStartingLifes = PlayerPrefs.GetInt("StartingLives", 3);
        maxUpgradeLifes = livesPrice.Count + defaultStartingLives;

        // Set upgrade life price
        UpdateUpgradeLifeText();

        // Set upgrade scope price
        UpdateUpgradeScopeText();

        // Set Coins
        UpdateCoins();
    }

    private void OnDisable()
    {
        ballPreview.SetActive(false);
    }

    void UpdateUpgradeScopeText()
    {
        actualScopeAccuracy = PlayerPrefs.GetInt("ScopeAccuracy", 30);

        upgradeScopeText.text = actualScopeAccuracy.ToString() + "%";

        switch(actualScopeAccuracy)
        {
            case 30:
                indexValScope = 0;
                break;
            case 40:
                indexValScope = 1;
                break;
            case 50:
                indexValScope = 2;
                break;
            case 60:
                indexValScope = 3;
                break;
            case 70:
                indexValScope = 4;
                break;
            case 80:
                indexValScope = 5;
                break;
            case 90:
                indexValScope = 6;
                break;
            case 100:
                indexValScope = -1;
                break;
        }

        if (indexValScope == -1)
            priceScopeText.text = "MAXIMO";
        else
        {
            int price = scopePrice[indexValScope];
            priceScopeText.text = price.ToString();
        }        
    }

    public void BuyUpgradeScopeAccuracy()
    {
        // Max upgrade reached
        if (indexValScope == -1)
            return;

        int price = scopePrice[indexValScope];

        // Not enough coins
        if (price > coins)
        {
            PlayInvalidSound();
            return;
        }            

        coins -= price;
        PlayerPrefs.SetInt("coins", coins);

        actualScopeAccuracy = scopeUpgrade[indexValScope];
        PlayerPrefs.SetInt("ScopeAccuracy", actualScopeAccuracy);

        UpdateUpgradeScopeText();
        UpdateCoins();

        PlaySuccessfulSound();
    }

    void UpdateUpgradeLifeText()
    {
        upgradeLifeText.text = actualStartingLifes.ToString() + " / " + maxUpgradeLifes.ToString();

        if (maxUpgradeLifes <= actualStartingLifes)
            priceLifeText.text = "-";
        else
            priceLifeText.text = livesPrice[actualStartingLifes - defaultStartingLives].ToString();
    }

    public void UpdateCoins()
    {
        coins = PlayerPrefs.GetInt("coins", 0);

        if(coinsText != null)
            coinsText.text = coins.ToString();

        if(buyMoreCoinsText != null)
            buyMoreCoinsText.text = coins.ToString();
    }

    public void BuyUpgradeLife()
    {
        // Max upgrade reached
        if (actualStartingLifes >= maxUpgradeLifes)
            return;

        int price = livesPrice[actualStartingLifes - defaultStartingLives];

        // Not enough coins
        if (coins < price)
        {
            PlayInvalidSound();
            return;
        }

        coins -= price;
        PlayerPrefs.SetInt("coins", coins);

        actualStartingLifes++;
        PlayerPrefs.SetInt("StartingLives", actualStartingLifes);
  
        UpdateUpgradeLifeText();
        UpdateCoins();

        PlaySuccessfulSound();
    }

    public void BackToMainMenu()
    {
        mainMenu.SetActive(true);
        shop.SetActive(false);
    }

    public void ChangeSkin(string _skinSelected)
    {
        skinSelected = _skinSelected;
        UpdateSkin();
    }

    void UpdateSkin()
    {
        // Destroy preview special functions
        foreach (Transform child in ball.transform)
        {
            Destroy(child.gameObject);
        }

        skin = DataManager.Instance.GetSkinDictionary(skinSelected);      

        previewBall.GetComponent<Renderer>().material = Resources.Load<Material>(skin.materialUrl);

        // Load new special functions
        if(skin.specialFunction)
        {
            Instantiate(Resources.Load<GameObject>(skin.specialFunctionUrl), ball.transform);
        }

        UpdateInterface();
    }

    void HideSkinTypes()
    {
        common.SetActive(false);
        rare.SetActive(false);
        epic.SetActive(false);
        legendary.SetActive(false);
    }

    void UpdateInterface()
    {
        string hasSkin = PlayerPrefs.GetString(skinSelected, "No");

        HideSkinTypes();

        switch (skin.type.ToLower())
        {
            case "common":
                common.SetActive(true);
                break;
            case "rare":
                rare.SetActive(true);
                break;
            case "epic":
                epic.SetActive(true);
                break;
            case "legendary":
                legendary.SetActive(true);
                break;
        }

        if (hasSkin == "Yes" || skinSelected == "SkinDefault")
        {
            if(skinSelected == actualSkin)
            {
                buyInterface.SetActive(false);
                aquiredInterface.SetActive(true);
                selectInterface.SetActive(false);
                adInterface.SetActive(false);
                specialInterface.SetActive(false);
            }
            else
            {
                buyInterface.SetActive(false);
                aquiredInterface.SetActive(false);
                selectInterface.SetActive(true);
                adInterface.SetActive(false);
                specialInterface.SetActive(false);
            }

            Color color = previewBall.GetComponent<Renderer>().material.color;
            color.a = (float)1;
            previewBall.GetComponent<Renderer>().material.color = color;
        }
        else
        {
            if (skin.ads)
            {
                buyInterface.SetActive(false);
                aquiredInterface.SetActive(false);
                selectInterface.SetActive(false);
                adInterface.SetActive(true);
                specialInterface.SetActive(false);

                UpdateAdInterface();
            }
            else if(!skin.purchasable)
            {
                buyInterface.SetActive(false);
                aquiredInterface.SetActive(false);
                selectInterface.SetActive(false);
                adInterface.SetActive(false);
                specialInterface.SetActive(true);

                UpdateSpecialInterface();
            }
            else
            {
                // Set up buy
                ballPrice.text = skin.price.ToString();

                Color color = previewBall.GetComponent<Renderer>().material.color;
                color.a = (float)1;
                previewBall.GetComponent<Renderer>().material.color = color;

                buyInterface.SetActive(true);
                aquiredInterface.SetActive(false);
                selectInterface.SetActive(false);
                adInterface.SetActive(false);
                specialInterface.SetActive(false);
            }            
        }
    }

    void UpdateAdInterface()
    {
        int sawAds = PlayerPrefs.GetInt(skinSelected + "_ads", 0);
        int totalAds = skin.price;
        int remainingAds = totalAds - sawAds;

        adsLeft.text = remainingAds.ToString();

        if(remainingAds <= 0)
        {
            PlayerPrefs.SetString(skinSelected, "Yes");

            ShowSelectedInterface();
        }
    }

    void UpdateSpecialInterface()
    {
        howToGetIt.text = skin.howToGetIt;
    }

    void ShowAdInterface()
    {
        buyInterface.SetActive(false);
        aquiredInterface.SetActive(false);
        selectInterface.SetActive(false);
        adInterface.SetActive(true);
    }

    void ShowBuyInterface()
    {
        buyInterface.SetActive(true);
        aquiredInterface.SetActive(false);
        selectInterface.SetActive(false);
        adInterface.SetActive(false);
    }

    void ShowAquiredInterfce()
    {
        buyInterface.SetActive(false);
        aquiredInterface.SetActive(true);
        selectInterface.SetActive(false);
        adInterface.SetActive(false);
    }

    void ShowSelectedInterface()
    {
        buyInterface.SetActive(false);
        aquiredInterface.SetActive(false);
        selectInterface.SetActive(true);
        adInterface.SetActive(false);
    }

    public void SelectSkin()
    {
        actualSkin = skinSelected;
        PlayerPrefs.SetString("SkinSelected", skinSelected);

        ShowAquiredInterfce();
    }

    public void BuySkin()
    {
        // Not enough coins
        if (coins < skin.price)
        {
            PlayInvalidSound();
            return;
        }
            
        coins -= skin.price;
        PlayerPrefs.SetInt("coins", coins);

        AnalyticsManager.Instance.TriggerEvent(skinSelected + " was purchased.");

        PlayerPrefs.SetString(skinSelected, "Yes");
        PlayerPrefs.SetString("SkinSelected", skinSelected);

        // Save skin in an string array
        string[] mySkins = PlayerPrefsX.GetStringArray("MySkins", "", 0);
        //Debug.Log("Number of skins owned before: " + mySkins.Length.ToString());
        
        mySkins = mySkins.Concat( new string[] { skinSelected }).ToArray();
        //Debug.Log("My skins: " + string.Join(", ", mySkins)); 

        PlayerPrefsX.SetStringArray("MySkins", mySkins);
        // Finish saving my skins

        actualSkin = skinSelected;

        Color color = previewBall.GetComponent<Renderer>().material.color;
        color.a = (float)1;
        previewBall.GetComponent<Renderer>().material.color = color;

        buyInterface.SetActive(false);
        aquiredInterface.SetActive(true);
        selectInterface.SetActive(false);

        PlaySuccessfulSound();

        UpdateCoins();
    }

    public void SeeAdForCoins(int _addCoins)
    {
        addCoins = _addCoins;        

        AdsManager.Instance.ReproduceAd(SuccessfulAdForCoins, FailureAd, "rewardedVideo");
    }

    private int addCoins;

    void SuccessfulAdForCoins()
    {
        coins = PlayerPrefs.GetInt("coins", 0);

        coins += addCoins;

        PlayerPrefs.SetInt("coins", coins);

        UpdateCoins();
    }

    public void SeeAd()
    {
        AdsManager.Instance.ReproduceAd(SuccessfulAd, FailureAd, "rewardedVideo");
    }    

    void SuccessfulAd()
    {
        int sawAds = PlayerPrefs.GetInt(skinSelected + "_ads", 0);
        sawAds++;
        PlayerPrefs.SetInt(skinSelected + "_ads", sawAds);

        UpdateAdInterface();
    }

    void FailureAd()
    {
        // Nothing
    }

    public void OpenBuyMoreCoins()
    {
        buyMoreCoins.SetActive(true);
        buySkins.SetActive(false);
        ballPreview.SetActive(false);

        UpdateCoins();
    }

    public void ReturnToShop()
    {
        buyMoreCoins.SetActive(false);
        buySkins.SetActive(true);
        ballPreview.SetActive(true);

        UpdateCoins();
    }

    void PlaySuccessfulSound()
    {
        audioSource.Stop();
        audioSource.clip = successfulPurchase;
        audioSource.Play();
    }

    void PlayInvalidSound()
    {
        audioSource.Stop();
        audioSource.clip = invalidPurchase;
        audioSource.Play();
    }

    // --------------------- Handle Buttons --------------------
    public void Buy500Coins(){
        IAPManager.Instance.SetCoinsText(buyMoreCoinsText);
        IAPManager.Instance.Buy500Coins();
    }

    public void Buy3000Coins(){
        IAPManager.Instance.SetCoinsText(buyMoreCoinsText);
        IAPManager.Instance.Buy3000Coins();
    }
    
    public void Buy7500Coins(){
        IAPManager.Instance.SetCoinsText(buyMoreCoinsText);
        IAPManager.Instance.Buy7500Coins();
    }

    public void BuyAdsRemover(){
        IAPManager.Instance.BuyAdsRemover();
    }
}
