﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundtrackManager : MonoBehaviour {

    public static SoundtrackManager Instance;

    private AudioSource audioSource;

    public AudioClip cityTrack;
    public AudioClip courtTrack;
    public AudioClip menuTrack;
    public AudioClip arcadePlayingTrack;
    public AudioClip arcadeLobbyTrack;
    public AudioClip arcadeFastforwardTrack;

    public float cityVolume;
    public float courtVolume;
    public float menuVolume;
    public float arcadePlayingVolume;
    public float arcadeLobbyVolume;
    public float arcadeFastforwardVolume;

    private AudioClip selectedTrack;
    private float selectedVolume;
    private float playingOffset;

    bool CheckIfDuplicate()
    {

        GameObject[] dontDestroyOnLoadObjects = GetDontDestroyOnLoadObjects.function();
        //Debug.Log("DontDestroyOnLoad GameObjects: " + dontDestroyOnLoadObjects.Length.ToString());

        foreach (GameObject gb in dontDestroyOnLoadObjects)
        {
            if (gb.name == gameObject.name)
                return true;
        }

        return false;
    }

    private void Awake()
    {
        if (CheckIfDuplicate())
            Destroy(gameObject);
        
        DontDestroyOnLoad(gameObject);

        Instance = this;
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PauseSoundtrack()
    {
        //Debug.Log("PauseSoundtrack()");
        audioSource.Pause();
    }

    public void ResumeSoundtrack()
    {
        audioSource.Play(0);
    }

    public void Play(string track)
    {
        track = track.ToLower();

        switch(track)
        {
            case "city":
                PlayCityTrack();
                break;
            case "court":
                PlayCourtTrack();
                break;
            case "menu":
                PlayMenuTrack();
                break;
            case "arcadeplaying":
                PlayArcadePlayingTrack();
                break;
            case "arcadelobby":
                PlayArcadeLobbyTrack();
                break;
            case "arcadefastworward":
                PlayArcadeFastforwardTrack();
                break;
        }
    }

    public void PlayMenuTrack()
    {
        selectedTrack = menuTrack;
        selectedVolume = menuVolume;
        PlayTrack();
    }

    public void PlayCityTrack()
    {
        selectedTrack = cityTrack;
        selectedVolume = cityVolume;
        PlayTrack();
    }

    public void PlayCourtTrack()
    {
        selectedTrack = courtTrack;
        selectedVolume = courtVolume;
        PlayTrack();
    }

    public void PlayArcadePlayingTrack()
    {
        selectedTrack = arcadePlayingTrack;
        selectedVolume = arcadePlayingVolume;
        PlayTrack();
    }

    public void PlayArcadeLobbyTrack(float _playingOffset = 0f)
    {
        playingOffset = _playingOffset % arcadeLobbyTrack.length;
        selectedTrack = arcadeLobbyTrack;
        selectedVolume = arcadeLobbyVolume;
        PlayTrack();
    }

    public void PlayArcadeFastforwardTrack()
    {
        selectedTrack = arcadeFastforwardTrack;
        selectedVolume = arcadeFastforwardVolume;
        PlayTrack();
    }

    void PlayTrack()
    {
        if (audioSource.clip == selectedTrack && audioSource.isPlaying)
            return;

        audioSource.Stop();
        audioSource.clip = selectedTrack;
        audioSource.volume = selectedVolume;

        if(playingOffset != 0f)
        {
            audioSource.time = playingOffset;
            playingOffset = 0f;
        }

        audioSource.Play();
    }

    public float GetPlayingTime()
    {
        return SoundtrackManager.Instance.audioSource.time;
    }
}
