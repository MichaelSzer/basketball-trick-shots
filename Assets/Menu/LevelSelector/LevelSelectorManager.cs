﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectorManager : MonoBehaviour {

    // -------- PACKS --------
    public GameObject tutorial;
    public GameObject firstSteps;
    public GameObject hardcore;
    public GameObject twoThrowsOneLife;
    public GameObject playOffs;
    public GameObject michaelJordan;

    // Use this for initialization
    private void OnEnable () {
        SelectPack();	
	}
	
	void SelectPack()
    {
        string pack = PlayerPrefs.GetString("PackSelected", "");
        PlayerPrefs.DeleteKey("PackSelected");

        if (pack == "Tutorial")
            ShowTutorial();
        else if (pack == "FirstSteps")
            ShowFirstSteps();
        else if (pack == "Hardcore")
            ShowHardcore();
        else if (pack == "TwoShootsOneLife")
            ShowTwoThrowsOneLife();
        else if (pack == "PlayOffs")
            ShowPlayOffs();
        else if (pack == "MichaelJordan")
            ShowMichaelJordan();
        else
            ShowTutorial();
    }

    void ConfigureAll()
    {
        tutorial.SetActive(false);
        firstSteps.SetActive(false);
        twoThrowsOneLife.SetActive(false);
        playOffs.SetActive(false);
        hardcore.SetActive(false);
        michaelJordan.SetActive(false);
    }

    public void ShowTutorial()
    {
        ConfigureAll();
        tutorial.SetActive(true);
    }

    public void ShowFirstSteps()
    {
        ConfigureAll();
        firstSteps.SetActive(true);
    }

    public void ShowTwoThrowsOneLife()
    {
        ConfigureAll();
        twoThrowsOneLife.SetActive(true);
    }

    public void ShowPlayOffs()
    {
        ConfigureAll();
        playOffs.SetActive(true);
    }

    public void ShowMichaelJordan()
    {
        ConfigureAll();
        michaelJordan.SetActive(true);
    }

    public void ShowHardcore()
    {
        ConfigureAll();
        hardcore.SetActive(true);
    }
}
