﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

[RequireComponent(typeof(RectTransform))]
public class LostScreen : MonoBehaviour {

    private AnalyticsTracker analyticsTracker;

    private void Awake()
    {
        RectTransform rt = GetComponent<RectTransform>();

        rt.sizeDelta = new Vector2(3200, 2400);
        rt.localPosition = new Vector3(5400, 0, 0);
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void ResetGame()
    {
        string pack = PlayerPrefs.GetString("PackSelected", "");

        string eventName = pack + " started.";
        AnalyticsManager.Instance.TriggerEvent(eventName);

        /*if (pack == "Tutorial")
            SceneManager.LoadScene("Tutorial_Level1");
        else if (pack == "Hardcore")
            SceneManager.LoadScene("Hardcore_Level1");
        else if (pack == "FirstSteps")
            SceneManager.LoadScene("FirstSteps_Level1");
        else if (pack == "TwoShootsOneLife")
            SceneManager.LoadScene("TwoShootsOneLife_Level1");
        else if (pack == "PlayOffs")
            SceneManager.LoadScene("PlayOffs_Level1");
        else if (pack == "MichaelJordan")
            SceneManager.LoadScene("MichaelJordan_Level1");
        else if (pack == "Hardcore")
            SceneManager.LoadScene("Hardcore_Level1");*/

        SceneManager.LoadScene(pack);
    }
}
