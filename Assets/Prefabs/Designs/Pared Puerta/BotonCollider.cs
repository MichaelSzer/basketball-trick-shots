﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonCollider : MonoBehaviour {

    public Animator wallAnimCont;
    Animator buttonAnimCont;

    bool activated = false;

    private void Awake()
    {
        buttonAnimCont = GetComponent<Animator> ();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (activated || collision.gameObject.name != "MainPlayer")
            return;

        activated = true;

        buttonAnimCont.Play("BotonPuertaActivandose");
        wallAnimCont.Play("ParedPuertaAbriendose");
    }

    public void Reset()
    {
        if(!this.gameObject.activeSelf)
            return;

        buttonAnimCont.Play("BotonPuertaQuieta");
        wallAnimCont.Play("ParedPuertaQuieta");

        activated = false;
    }
}
