﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour {

    public GameObject respawnPoint;
    public Portal portalDestination;
    public GameObject boxColliders;
    private int collidersTriggered = 0;
    private bool teleporting = false;

    public Vector3 portalPosition()
    {
        return respawnPoint.transform.position;
    }

    public void DisableColliders()
    {
        boxColliders.SetActive(false);
        collidersTriggered = 2;
        teleporting = true;
    }

    public void ActiveColliders()
    {
        boxColliders.SetActive(true);
        collidersTriggered = 0;
        teleporting = false;
    }

    public void EnteringPortal(GameObject mainPlayer)
    {
        mainPlayer.transform.position = portalDestination.portalPosition();
        portalDestination.DisableColliders();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name != "MainPlayer" || teleporting)
            return;

        collidersTriggered++;

        if (collidersTriggered >= 2)
            EnteringPortal(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name != "MainPlayer")
            return;

        collidersTriggered--;

        if(collidersTriggered <= 0)
            ActiveColliders();
    }
}
