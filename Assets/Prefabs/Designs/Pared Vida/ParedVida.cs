﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class ParedVida : MonoBehaviour {

    BallControlling ball;

	// Use this for initialization
	void Awake () {

        // Get ball
        ball = GameObject.Find("MainPlayer").GetComponent<BallControlling> ();
	}

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.name != "MainPlayer")
            return;

        ball.AnotherShoot();
    }
}
