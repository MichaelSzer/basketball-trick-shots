﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCoins : MonoBehaviour {

	public GameObject coinsParent;

	void OnEnable()
	{
		foreach(Transform coin in coinsParent.transform)
			Destroy(coin.gameObject);
	}
}
