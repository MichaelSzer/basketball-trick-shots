﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class ManageNewHighscoreRewards : MonoBehaviour {

	public GameObject coin;
	public GameObject coinsParent;

	public GameObject fastforwardButton;
	bool fastforward = false; 

	public float rateSpawn;

	public int reward1Goal = 5;
	public int reward2Goal = 15;
	public int reward3Goal = 30;
	public int reward4Goal = 45;
	public int reward5Goal = 65;
	public int reward6Goal = 75;

	public TimelineAsset reward1Timeline;
	public TimelineAsset reward2Timeline;
	public TimelineAsset reward3Timeline;
	public TimelineAsset reward4Timeline;
	public TimelineAsset reward5Timeline;
	public TimelineAsset reward6Timeline;

	PlayableDirector playableDirector;

	int score;
	int coinsRewardLeft = 0;

	void OnEnable()
	{
		if(playableDirector == null)
			playableDirector = GetComponent<PlayableDirector> ();

		ManageRewards();
	}

	void ManageRewards(){

		if(PlayerPrefs.GetInt("ArcadeMode_Reward1Unlocked", 0) == 0 && score >= reward1Goal)
			StartCoroutine(GiveReward(reward1Timeline, "ArcadeMode_Reward1Unlocked", 25));
		else if(PlayerPrefs.GetInt("ArcadeMode_Reward2Unlocked", 0) == 0 && score >= reward2Goal)
			StartCoroutine(GiveReward(reward2Timeline, "ArcadeMode_Reward2Unlocked", 0, "SkinArcade1"));
		else if(PlayerPrefs.GetInt("ArcadeMode_Reward3Unlocked", 0) == 0 && score >= reward3Goal)
			StartCoroutine(GiveReward(reward3Timeline, "ArcadeMode_Reward3Unlocked", 100));
		else if(PlayerPrefs.GetInt("ArcadeMode_Reward4Unlocked", 0) == 0 && score >= reward4Goal)
			StartCoroutine(GiveReward(reward4Timeline, "ArcadeMode_Reward4Unlocked", 0, "SkinArcade2"));
		else if(PlayerPrefs.GetInt("ArcadeMode_Reward5Unlocked", 0) == 0 && score >= reward5Goal)
			StartCoroutine(GiveReward(reward5Timeline, "ArcadeMode_Reward5Unlocked", 250));
		else if(PlayerPrefs.GetInt("ArcadeMode_Reward6Unlocked", 0) == 0 && score >= reward6Goal)
			StartCoroutine(GiveReward(reward6Timeline, "ArcadeMode_Reward6Unlocked", 0, "SkinArcade3"));
		else
			TimelineManager.Instance.PlayNewHighscoreFinalCutscene();
	}

	public bool RewardToClaim(int _score)
	{
		score = _score;

		if((PlayerPrefs.GetInt("ArcadeMode_Reward1Unlocked", 0) == 0 && score >= reward1Goal) ||
			(PlayerPrefs.GetInt("ArcadeMode_Reward2Unlocked", 0) == 0 && score >= reward2Goal) ||
			(PlayerPrefs.GetInt("ArcadeMode_Reward3Unlocked", 0) == 0 && score >= reward3Goal) ||
			(PlayerPrefs.GetInt("ArcadeMode_Reward4Unlocked", 0) == 0 && score >= reward4Goal) ||
			(PlayerPrefs.GetInt("ArcadeMode_Reward5Unlocked", 0) == 0 && score >= reward5Goal) ||
			(PlayerPrefs.GetInt("ArcadeMode_Reward6Unlocked", 0) == 0 && score >= reward6Goal))
			return true;
		else
			return false;
	}	

	IEnumerator GiveReward(TimelineAsset timeline, string reward, int coins = 0, string skin = "")
	{
		playableDirector.Play(timeline);

		while(playableDirector.state == PlayState.Playing){
			yield return null;
		}

		if(coins > 0)
		{
			StartCoroutine(SpawnRewardCoins(coins));
		}
		else if(skin != "")
		{
			// Unlock skin
			PlayerPrefs.SetString(skin, "Yes");
		}

		PlayerPrefs.SetInt(reward, 1);

		if(coins == 0)
			ManageRewards();
	}

	int CalculateCoinsRewards()
	{
		int newHighscore = PlayerPrefs.GetInt("ArcadeMode_highscore", 0);
		int oldHighscore = PlayerPrefs.GetInt("ArcadeMode_highscoreRewarded", 0);

		// Update new highscore
		PlayerPrefs.SetInt("ArcadeMode_highscoreRewarded", newHighscore);		

		int coinsReward = 0;

		for(int i = oldHighscore + 1; i <= newHighscore; i++)
		{
			if(i < 30)
			{
				coinsReward += 1;	
			}
			else if(i < 50)
			{
				coinsReward += 2;
			}
			else if(i < 60)
			{
				coinsReward += 4;	
			}
			else if(i < 70)
			{
				coinsReward += 8;	
			}
			else if(i < 75)
			{
				coinsReward += 16;	
			}
			else if(i < 80)
			{
				coinsReward += 20;	
			}
			else
			{
				coinsReward += 20 + i - 80;
			}
		}

		return coinsReward;
	}

	IEnumerator SpawnRewardCoins(int coins)
	{
		coinsRewardLeft = coins;

		if(coinsRewardLeft > 28)
			fastforwardButton.SetActive(true);
		else if(fastforwardButton.activeSelf)
			fastforwardButton.SetActive(false);

		WaitForSeconds waitRateSpawn = new WaitForSeconds(1f / rateSpawn);

		int actualCoins = PlayerPrefs.GetInt("coins", 0);
		int finalCoins = actualCoins + coins;

		while(coinsRewardLeft > 0)
		{
			if(coinsRewardLeft <= 20 && fastforwardButton.activeSelf)
				fastforwardButton.SetActive(false);

			actualCoins = PlayerPrefs.GetInt("coins", 0);

			if(finalCoins - actualCoins - 1 == 4)
				coinsRewardLeft = 4;

			if(coinsRewardLeft <= 4 && fastforward)
			{
				SoundtrackManager.Instance.PlayArcadeLobbyTrack(playingTime);
				Time.timeScale = 1f;
			}

			coinsRewardLeft--;

			GameObject newCoin = Instantiate(coin, coinsParent.transform);
			newCoin.SetActive(true);

			yield return waitRateSpawn;
		}

		yield return new WaitForSeconds(2f);

		ManageRewards();
	}

	public float playingTime;

	public void FastforwardReward()
	{
		playingTime = SoundtrackManager.Instance.GetPlayingTime();

		// Change music to fastforward
		SoundtrackManager.Instance.PlayArcadeFastforwardTrack();

		float secondsMin = 3f;
		float secondsToAdd = (float)(coinsRewardLeft / 10) / 10;
		float totalSeconds = secondsMin + secondsToAdd;

		float timeScale = coinsRewardLeft / (totalSeconds * rateSpawn);		

		if(timeScale > 20f)
			timeScale = 20f;

		Time.timeScale = timeScale;
		fastforward = true;
		fastforwardButton.SetActive(false);
	}

	void OnApplicationQuit()
	{
		int actualCoins = PlayerPrefs.GetInt("coins", 0);
		actualCoins += coinsRewardLeft;
		PlayerPrefs.SetInt("coins", actualCoins);
	}
}
