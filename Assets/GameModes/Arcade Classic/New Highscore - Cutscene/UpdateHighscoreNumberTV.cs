﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateHighscoreNumberTV : MonoBehaviour {

	public TextMesh highscoreNumberText;

	void OnEnable()
	{
		int highscore = PlayerPrefs.GetInt("ArcadeMode_highscore", 0);
		highscoreNumberText.text = highscore.ToString();
	}
}
