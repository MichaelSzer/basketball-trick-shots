﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class DetectCollisions : MonoBehaviour {

	public Text textCoins;
	AudioSource audioSource;
	Rigidbody rb;

	//public AudioClip spawningSound;
	//public float spawningVol;

	public List<AudioClip> hittingPlatterSound;
	public float hittingPlatterVol;	

	void OnEnable()
	{
		rb = GetComponent<Rigidbody> ();

		audioSource = GetComponent<AudioSource> ();

		// Play machine respawn coin
		/*audioSource.clip = spawningSound;
		audioSource.volume = spawningVol;
		audioSource.Play();*/
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "AddCoin")
		{
			int coins = PlayerPrefs.GetInt("coins", 0);
			coins++;
			PlayerPrefs.SetInt("coins", coins);
			textCoins.text = coins.ToString();
		}
		else if(other.name == "CoinsDeadTrigger")
		{
			Destroy(this.gameObject);
		}
	}

	float maxVel = 7f; 
	float minVel = 0.2f;
	float volMagnifier = 1.2f;

	void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.tag == "GoldenPlate" && rb != null)
		{
			if(Mathf.Abs(rb.velocity.y) < minVel)
				return;

			//Debug.Log(Mathf.Abs(rb.velocity.y));

			int randomAudio = Random.Range(0, hittingPlatterSound.Count);

			// Play hitting golden plate
			audioSource.clip = hittingPlatterSound[randomAudio];
			audioSource.volume = hittingPlatterVol * volMagnifier * (Mathf.Abs(rb.velocity.y) / maxVel);
			audioSource.Play();
		}
	}
}
