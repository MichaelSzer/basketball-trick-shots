﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushMilkCoin : MonoBehaviour {

	Rigidbody rb;

	public float ROTATION_ANGLE = 100f;
	public float PUSH_FORCE = 300f;
	public float PUSH_FORCE_RANDOM = 100f;

	void OnEnable()
	{
		RotateCoin();
		AddForceToCoin();	
	}

	float timerToDestroyRB = 0f;

	void Update()
	{
		timerToDestroyRB += Time.deltaTime;

		if(timerToDestroyRB >= 8f)
		{
			if(gameObject.activeSelf)
				Destroy(rb);
		}	
	}

	int randomMaxRotate = 1000;

	void RotateCoin()
	{
		float random = (float)(Random.Range(1, randomMaxRotate) - (randomMaxRotate / 2)) / randomMaxRotate;

		transform.Rotate(transform.up * ROTATION_ANGLE * random);
		//transform.Translate(transform.right * 1 * OFFSET_Z);
	}

	int randomMaxForce = 1000;

	void AddForceToCoin()
	{
		rb = GetComponent<Rigidbody> ();

		if(rb == null)
			rb = gameObject.AddComponent<Rigidbody> ();	


		float random = (float)(Random.Range(1, randomMaxForce) - (randomMaxForce / 2)) / randomMaxForce;

		rb.AddForce(transform.right * (PUSH_FORCE + PUSH_FORCE_RANDOM * random) );
	}
}
