﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayArcadeLobbyTrack : MonoBehaviour {

	void OnEnable()
	{
		SoundtrackManager.Instance.PlayArcadeLobbyTrack();
	}

	/*void OnDisable()
	{
		SoundtrackManager.Instance.PauseSoundtrack();
	}*/
}
