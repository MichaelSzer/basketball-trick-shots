﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour {

	void OnEnable()
	{
		ClassicGameBrain.Instance.StartGame();
	}
}
