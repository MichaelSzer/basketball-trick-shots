﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassicGameBallRespawner : MonoBehaviour {

	public static ClassicGameBallRespawner Instance;

	float respawnRate = 0.2f;
	bool ballShooted = false;
	bool activateSpawn = false, spawn = false;

	public int maxRespawnBalls = 5;
	private int ballsRemaining = 0;

	public GameObject ball;
	public GameObject ballParent;
	private GameObject ballSpawner;

	float respawnTimer;

	public Cloth netCloth;
	private ClothSphereColliderPair[] sphereColliders;
	private int colliderIndex, colliderSize = 10;

	void Awake()
	{
		Instance = this;
	}

	/*void Start()
	{		
		Configure();

		spawn = true;
		SpawnBall();
	}*/

	void Configure()
	{
		respawnTimer = 0f;
		ballsRemaining = 3;
		colliderIndex = 0;
		sphereColliders = new ClothSphereColliderPair[colliderSize];
	}

	void Update()
	{
		if(!ClassicGameBrain.Instance.IsTimerStarted())
			return;

		respawnTimer += Time.deltaTime;
			
		if(respawnTimer > respawnRate)
		{
			respawnTimer = 0f;

			ballsRemaining++;
			if(ballsRemaining > maxRespawnBalls)
				ballsRemaining = maxRespawnBalls;
		}

		if(ClassicGameBrain.Instance.WasBallShooted())
		{
			if(!activateSpawn)
			{
				ActivateSpawn();
			}

			if(ballsRemaining > 0)
				SpawnBall();
		}
	}

	void SetSpawn()
	{
		spawn = true;
	}

	void ActivateSpawn()
	{
		activateSpawn = true;
		Invoke("SetSpawn", 0.2f);
	}

	void SpawnBall()
	{
		if(!spawn)
			return;

		ballsRemaining--;
		activateSpawn = false;
		spawn = false;

		ClassicGameBrain.Instance.BallSpawned();

		GameObject ballSpawned = Instantiate(ball, ballSpawner.transform.position, Quaternion.identity, ballParent.transform);
		ballSpawned.SetActive(true);
		ballSpawned.AddComponent<BallControllingClassic> ();
		ballSpawned.GetComponent<BallControllingClassic> ().freezePosition2D = true;

		//if(!ClassicGameBrain.Instance.IsTimerStarted())
		//	ballSpawned.GetComponent<BallControllingClassic> ().firstBall = true;

		sphereColliders[colliderIndex] = new ClothSphereColliderPair(ballSpawned.GetComponent<SphereCollider> ());

		colliderIndex++;
		if(colliderIndex == colliderSize)
			colliderIndex = 0;

		netCloth.sphereColliders = sphereColliders;
	}

	void DestroyBalls()
	{
		foreach (Transform childBall in ballParent.transform)
			Destroy(childBall.gameObject);
	}

	public void SetSpawner(GameObject spawner)
	{
		ballSpawner = spawner;
	}

	public void RestartGame()
	{
		Configure();
		DestroyBalls();

		spawn = true;
		SpawnBall();
	}
}
