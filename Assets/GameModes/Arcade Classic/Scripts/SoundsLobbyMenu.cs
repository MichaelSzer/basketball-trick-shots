﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundsLobbyMenu : MonoBehaviour {

	public static SoundsLobbyMenu Instance;
	public AudioClip audioSuccessPay, audioFailurePay;
	private AudioSource audioSource;


	void Awake()
	{
		if(Instance == null)
		{
			Instance = this;
		}
		else
		{
			Destroy(gameObject);
		}

		audioSource = GetComponent<AudioSource> ();
	}

	public void PlaySuccessPay()
	{
		audioSource.clip = audioSuccessPay;
		audioSource.Play();
	}

	public void PlayFailurePay()
	{
		audioSource.clip = audioFailurePay;
		audioSource.Play();
	}
}
