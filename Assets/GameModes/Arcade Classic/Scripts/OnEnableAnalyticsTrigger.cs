﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnableAnalyticsTrigger : MonoBehaviour {

	public string nameEvent;

	void OnEnable()
	{
		AnalyticsManager.Instance.TriggerEvent(nameEvent);
	}
}
