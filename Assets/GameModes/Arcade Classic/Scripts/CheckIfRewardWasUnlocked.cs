﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckIfRewardWasUnlocked : MonoBehaviour {

	public GameObject unlocked;
	public string reward;

	void Awake()
	{
		if(PlayerPrefs.GetInt(reward, 0) == 1)
		{
			unlocked.SetActive(true);
		}
		else
		{
			unlocked.SetActive(false);	
		}
	}
}
