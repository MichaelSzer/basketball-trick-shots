﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlArcadeAnimations : MonoBehaviour {

	public List<AnimateScreenColor> animations;

	void OnEnable()
	{
		foreach(AnimateScreenColor animation in animations)
		{
			animation.StartAnimation();
		}
	}

	void OnDisable()
	{
		foreach(AnimateScreenColor animation in animations)
		{
			animation.StopAnimation();
		}
	}
}
