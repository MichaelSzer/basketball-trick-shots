﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayArcadePlayingSoundtrack : MonoBehaviour {

	void OnEnable()
	{
		SoundtrackManager.Instance.PlayArcadePlayingTrack();
	}
}
