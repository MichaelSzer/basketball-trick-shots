﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PayTicketButton : MonoBehaviour {

	int ticketPrice = 5;

	public void PayTicket()
	{
		int coins = PlayerPrefs.GetInt("coins", 0);

		if(coins >= ticketPrice)
		{
			// Pause soundtrack
			//SoundtrackManager.Instance.PauseSoundtrack();

			TimelineManager.Instance.PlayInsertingCoinsCutscene();

			// Play valid purchase sound
			SoundsLobbyMenu.Instance.PlaySuccessPay();

			AnalyticsManager.Instance.TriggerEvent("Arcade ticket paid.");
		}
		else
		{
			// Play invalid purchase sound
			SoundsLobbyMenu.Instance.PlayFailurePay();
		}
	}
}
