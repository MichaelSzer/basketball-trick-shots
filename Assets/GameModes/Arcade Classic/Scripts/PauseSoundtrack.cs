﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseSoundtrack : MonoBehaviour {

	void OnEnable()
	{
		SoundtrackManager.Instance.PauseSoundtrack();
	}
}
