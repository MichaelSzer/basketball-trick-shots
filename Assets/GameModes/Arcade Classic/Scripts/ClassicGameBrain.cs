﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class ClassicGameBrain : MonoBehaviour {

	public static ClassicGameBrain Instance;

	public TextMesh timeleftText, scoreText, highscoreText;
	public float initialTimer = 60f;
	private float timeleft;
	private int score, addScore, ballsShooted;
	private bool gameStarted = true, timerStarted = false, ballShooted = false;

	private float difficulty = 1;

	public List<GameObject> spawnersPosition;

	public ManageNewHighscoreRewards manageNewHighscoreRewards;

	void Awake()
	{
		Instance = this;
	}

	public void StartGame()
	{
		StartTimer();

		AnalyticsManager.Instance.TriggerEvent("Arcade game started.");
	}

	void Update()
	{
		if(timerStarted)
		{
			timeleft -= Time.deltaTime;

			if(timeleft < 0f)
			{
				timeleft = 0f;
				timerStarted = false;

				// Play final whisitle sound
				SoundsInGameManager.Instance.PlayWhistle();

				Invoke("FinishGame", 1f);
			}

			UpdateUI();
			ChangeDifficulty();
		}	
	}

	private void ConfigureGame()
	{
		// Set variables
		timeleft = initialTimer;
		timerStarted = false;
		score = 0;
		gameStarted = true;
		ballsShooted = 0;

		// Set UI
		UpdateUI();

		ClassicGameBallRespawner.Instance.SetSpawner(spawnersPosition[0]);
		ClassicGameBallRespawner.Instance.RestartGame();
	}

	void ChangeDifficulty()
	{
		if(timeleft <= 20f && difficulty < 3)
			ActivateDifficultThree();
		else if(timeleft <= 40f && difficulty < 2)
			ActivateDifficultTwo();
		else if(timeleft <= 60f && difficulty < 1)
			ActivateDifficultOne();
	}

	void ActivateDifficultOne()
	{
		difficulty = 1;
		addScore = 1;
		ClassicGameBallRespawner.Instance.SetSpawner(spawnersPosition[0]);
		HoopControlManager.Instance.ActivateMoveOne();
		LedsManager.Instance.SetLedsLoop(10, 2);
	}

	void ActivateDifficultTwo()
	{
		difficulty = 2;
		addScore = 2;
		//ClassicGameBallRespawner.Instance.SetSpawner(spawnersPosition[1]);
		HoopControlManager.Instance.ActivateMoveTwo();
		LedsManager.Instance.SetLedsLoop(18, 4);
	}

	void ActivateDifficultThree()
	{
		difficulty = 3;
		addScore = 3;
		//ClassicGameBallRespawner.Instance.SetSpawner(spawnersPosition[2]);
		HoopControlManager.Instance.ActivateMoveThree();
		LedsManager.Instance.SetLedsLoop(26, 6);
	}

	void UpdateUI()
	{
		timeleftText.text = ((int)timeleft).ToString();
		scoreText.text = score.ToString();

		int highscore = PlayerPrefs.GetInt("ArcadeMode_highscore", 0);
		highscoreText.text = highscore.ToString();
	}

	void ScoreAnimation()
	{
		if(!LedsManager.Instance.IsActive())
			return;

		if(addScore == 3)
		{
			LedsManager.Instance.StartTiltLeds(3);
		}
		else if(addScore == 2)
		{
			LedsManager.Instance.StartTiltLeds(2);
		}
		else if(addScore == 1)
		{
			LedsManager.Instance.StartTiltLeds(1);
		}
	}

	void ScoreSoundEffect()
	{
		if(addScore == 3)
		{
			SoundsInGameManager.Instance.PlayTriplePoint();
		}
		else if(addScore == 2)
		{
			SoundsInGameManager.Instance.PlayDoublePoint();
		}
		else if(addScore == 1)
		{
			SoundsInGameManager.Instance.PlayOnePoint();
		}
	}

	public int GetScore()
	{
		return score;
	}

	public void Score(string typeOfBall)
	{
		score += addScore;

		int highscore = PlayerPrefs.GetInt("ArcadeMode_highscore", 0);
		if(score > highscore)
			PlayerPrefs.SetInt("ArcadeMode_highscore", score);

		ScoreSoundEffect();
		ScoreAnimation();
		UpdateUI();
	}

	public void RestartGame()
	{
		ConfigureGame();
		HoopControlManager.Instance.RestartGame();
	}

	public void StartTimer()
	{
		if(IsGameStarted()){
			
			ActivateDifficultOne();
			timerStarted = true;
		}
	}

	public bool IsTimerStarted()
	{
		return initialTimer > timeleft && gameStarted;
	}

	void FinishGame()
	{
		timerStarted = false;
		gameStarted = false;
		HoopControlManager.Instance.FinishGame();

		int newHighscore = PlayerPrefs.GetInt("ArcadeMode_highscore", 0);
		int oldHighscore = PlayerPrefs.GetInt("ArcadeMode_highscoreRewarded", 0);

		SoundtrackManager.Instance.PlayArcadeLobbyTrack();

		AnalyticsManager.Instance.TriggerEvent("Arcade game finished.");

		// ------------- Analytics information -------------
        string skinSelected = PlayerPrefs.GetString("SkinSelected", "SkinDefault");
        int timesPlayed = PlayerPrefs.GetInt("ArcadeMode_timesPlayed", 0);
        int coinsWon = (int)(score * RemoteTunningVariables.Instance.ArcadeCoinsPerPoint);

        Dictionary<string, object> dataAnalytics = new Dictionary<string, object>
        {
            {"skin_selected", skinSelected},
            {"balls_shooted", ballsShooted},
            {"score", score},
            {"times_played", timesPlayed},
            {"coins_won_without_highscore_reward", coinsWon}
        };

        AnalyticsEvent.Custom("arcade_game_completed", dataAnalytics);

        timesPlayed++;
        PlayerPrefs.SetInt("ArcadeMode_timesPlayed", timesPlayed);
        // ------------- Finish uploading analytics -------------

        if(newHighscore > oldHighscore)
        	PlayerPrefs.SetInt("ArcadeMode_highscoreRewarded", newHighscore);

		if(manageNewHighscoreRewards.RewardToClaim(score))
			TimelineManager.Instance.PlayNewHighscoreCutscene();
		else
			TimelineManager.Instance.PlayFinishingGameCutscene();		
	}

	public void ShootBall()
	{
		ballsShooted++;
		ballShooted = true;
	}

	public void BallSpawned()
	{
		ballShooted = false;
	}

	public bool WasBallShooted()
	{
		return ballShooted;
	}

	public bool IsGameStarted()
	{
		return gameStarted;
	}
}
