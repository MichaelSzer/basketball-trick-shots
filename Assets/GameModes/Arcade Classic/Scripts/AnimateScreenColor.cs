﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AnimateScreenColor : MonoBehaviour {

	[System.Serializable]
	public class ColorSequence 
	{
		public List<Color> colorList;
		public float sequenceTime;
	}

	Material mat;	
	Color initialColor;

	/*AudioSource audioSource;
	public List<AudioClip> machineSounds;  
	float machineVol = 0.05f;
	float startingVol = 0.025f;*/

	void Awake()
	{
		mat = GetComponent<Renderer> ().material;
		initialColor = mat.color;

		/*audioSource = GetComponent<AudioSource> ();

		if(audioSource == null)
			audioSource = gameObject.AddComponent<AudioSource> ();*/
	}

	Coroutine co;

	public void StartAnimation()
	{
		co = StartCoroutine(PlaySequence(startSequence));
	}
	
	public void StopAnimation()
	{
		StopCoroutine(co);
		mat.SetColor ("_EmissionColor", initialColor);
	}

	public ColorSequence startSequence;
	public ColorSequence sequenceOne;
	public ColorSequence sequenceTwo;
	public ColorSequence sequenceThree;
	public ColorSequence sequenceFour;
	public ColorSequence sequenceFive;

	int numberOfSequence = 5;

	bool startingSequence = true;

	void ChooseRandomSequence()
	{
		int random = Random.Range(1, numberOfSequence+1);

		switch(random)
		{
			case 1:
				co = StartCoroutine(PlaySequence(sequenceOne));
				break;
			case 2:
				co = StartCoroutine(PlaySequence(sequenceTwo));
				break;
			case 3:
				co = StartCoroutine(PlaySequence(sequenceThree));
				break;
			case 4:
				co = StartCoroutine(PlaySequence(sequenceFour));
				break;
			case 5:
				co = StartCoroutine(PlaySequence(sequenceFive));
				break;
		}
	}

	IEnumerator PlaySequence(ColorSequence colorSequence)
	{
		float deltaWait = colorSequence.sequenceTime / colorSequence.colorList.Count;

		WaitForSeconds delayBetweenColor = new WaitForSeconds(deltaWait);

		foreach(Color color in colorSequence.colorList)
		{
			mat.SetColor ("_EmissionColor", color);

			// Play random audio clip
			/*int random = Random.Range(0, machineSounds.Count);
			audioSource.clip = machineSounds[random];

			if(startingSequence)
				audioSource.volume = startingVol;
			else
				audioSource.volume = machineVol;

			audioSource.Play();*/

			yield return delayBetweenColor;
		}

		startingSequence = false;

		ChooseRandomSequence();

		yield return null; 
	}	
}
