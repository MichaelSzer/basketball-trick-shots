﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartGame : MonoBehaviour {

	void OnEnable()
	{
		//Debug.Log("RestartGame()");
		ClassicGameBrain.Instance.RestartGame();
	}
}
