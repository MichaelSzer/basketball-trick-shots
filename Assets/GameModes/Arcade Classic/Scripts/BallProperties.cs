﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BallProperties : MonoBehaviour {

	private bool triggerOne = false, triggerTwo = false;
	private string ball = "normal";
	private bool activated = true; 	
	public float timeToDestroy = 3f, minimumTimeAlive = 1f, maxTimeAlive = 10f, minimumSpeed = 0.3f;
	private int isOnFloor = 0;
	private float timeOnFloor;
	private float timerFloorBeforeDestroying = 2f;

	float timeAlive = 0f;

	private Rigidbody rigidbody;

	private BallControllingClassic ballControllingClassic;

	/*private int id;

	public void AssignID(int _id)
	{
		id = _id;
	}*/

	void Start()
	{
		rigidbody = GetComponent<Rigidbody> ();
		ballControllingClassic = GetComponent<BallControllingClassic> ();
	}

	void Update()
	{
		//Debug.Log(rigidbody.velocity.magnitude);
		//Debug.Log(timeAlive);

		if(!ballControllingClassic.Shooted())
			return;

		if(isOnFloor > 0)
			timeOnFloor += Time.deltaTime;
		else
			timeOnFloor = 0f; 

		timeAlive += Time.deltaTime;
		if((timeOnFloor > timerFloorBeforeDestroying || timeAlive > maxTimeAlive) && activated)
			DestroyItself();
	}

	void CheckScore()
	{
		if(!triggerOne || !triggerTwo || !activated)
			return;

		ClassicGameBrain.Instance.Score(ball);

		activated = false;
		Invoke("DestroyItself", timeToDestroy);
	}

	void DestroyItself()
	{
		if(minimumTimeAlive < timeAlive)
			Destroy(gameObject);
	}

	void OnFloor()
	{
		isOnFloor++;
	}

	void OffFloor()
	{
		isOnFloor--;
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "DieCollider")
			DestroyItself();

		//if(other.tag == "OutsideBound")
		//	SoundsInGameManager.Instance.PlayOutsideShoot();
	}

	void OnTriggerStay(Collider other)
	{
		if(rigidbody.velocity.y > 0f)
			return;

		if(other.tag == "HoopCollider1")
			triggerOne = true;
		else if(other.tag == "HoopCollider2")
			triggerTwo = true;

		CheckScore();	
	}

	void OnCollisionEnter(Collision other)
	{
		SoundsInGameManager.Instance.PlayBounceEffect(rigidbody.velocity);

		if(other.gameObject.tag == "Floor")
			OnFloor();
	}

	void OnCollisionExit(Collision other)
	{
		if(other.gameObject.tag == "Floor")
			OffFloor();
	}
}
