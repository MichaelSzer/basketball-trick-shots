﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LedsManager : MonoBehaviour {

	public static LedsManager Instance;

	public GameObject ledsParent;
	List<GameObject> leds;

	void Awake()
	{
		if(Instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			Instance = this;
		}


		float tiltDelay = 0.4f;
		delayTilt = new WaitForSeconds(tiltDelay);
	}

	void AddChildsToList()
	{
		leds = new List<GameObject> ();

		foreach(Transform led in ledsParent.transform)
		{
			leds.Add(led.gameObject);
		}
	}

	void TurnOnLeds()
	{
		foreach(GameObject led in leds)
			led.SetActive(true);
	}

	void TurnOffLeds()
	{
		foreach(GameObject led in leds)
			led.SetActive(false);
	}

	int timesToTilt;
	bool ledsTilting = false;

	WaitForSeconds delayTilt;

	IEnumerator TiltLeds()
	{
		ledsTilting = true;

		int actualTiltTimes = 0;

		while(actualTiltTimes < timesToTilt)
		{
			actualTiltTimes++;

			TurnOnLeds();
			yield return delayTilt;
			TurnOffLeds();
			yield return delayTilt;
		}		

		ledsTilting = false;
	}

	public bool IsActive()
	{
		return gameObject.activeSelf;
	}

	public void StartTiltLeds(int times)
	{
		timesToTilt = times;
		coroutineTiltLeds = StartCoroutine(TiltLeds());
	}

	public float loopVel = 3f;
	public int sizeLedsTilting = 3;

	public void SetLedsLoop(float vel, int size)
	{
		loopVel = vel;
		sizeLedsTilting = size;

		delayLoop = new WaitForSeconds(1f / loopVel);
	}

	private int index = 0;
	WaitForSeconds delayLoop;

	IEnumerator LedsLoop()
	{
		while(true)
		{
			// If leds are tilting pause this coroutine
			if(ledsTilting)
			{
				yield return null;
				continue;
			}

			// Turn off previous led
			if(index == 0)
				leds[leds.Count-1].SetActive(false);
			else
				leds[index-1].SetActive(false);

			// Turn on new leds
			int tempIndex = index;
			int steps = 0;

			while(steps < sizeLedsTilting)
			{
				if(!leds[tempIndex].activeSelf)
					leds[tempIndex].SetActive(true);

				steps++;
				tempIndex++;

				if(tempIndex == leds.Count)
					tempIndex= 0;
			}

			index++;

			if(index == leds.Count)
				index = 0;

			yield return delayLoop;
		}
		
	}

	Coroutine coroutineLedsLoop;
	Coroutine coroutineTiltLeds;

	void OnEnable()
	{
		if(leds == null)
			AddChildsToList();

		coroutineLedsLoop = StartCoroutine(LedsLoop());
	}

	void OnDisable()
	{
		StopCoroutine(coroutineLedsLoop);

		if(coroutineTiltLeds != null)
			StopCoroutine(coroutineTiltLeds);
	
		TurnOffLeds();
	}
}
