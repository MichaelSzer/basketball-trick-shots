﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineManager : MonoBehaviour {

	public static TimelineManager Instance;
	public GameObject lobbyTimeline, insertingCoinsTimeline, playingTimeline, finishingTimeline, newHighscoreTimeline, newHighscoreFinalTimeline;
	PlayableDirector playableDirector;

	void Awake()
	{
		if(Instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			Instance = this;
		}
	}

	void HideAllTimelines()
	{
		lobbyTimeline.SetActive(false);
		insertingCoinsTimeline.SetActive(false);
		playingTimeline.SetActive(false);
		finishingTimeline.SetActive(false);
		newHighscoreTimeline.SetActive(false);
		newHighscoreFinalTimeline.SetActive(false);
	}

	void RestartTimeline()
	{
		playableDirector.Stop();
		playableDirector.time = 0f;
		playableDirector.Play();
	}

	public void PlayInsertingCoinsCutscene()
	{
		HideAllTimelines();
		insertingCoinsTimeline.SetActive(true);
		playableDirector = insertingCoinsTimeline.GetComponent<PlayableDirector> ();
		RestartTimeline();
	}

	public void PlayLobbyCutscene()
	{
		HideAllTimelines();
		lobbyTimeline.SetActive(true);
		playableDirector = lobbyTimeline.GetComponent<PlayableDirector> ();
		RestartTimeline();
	}

	public void PlayingGameCutscene()
	{
		HideAllTimelines();
		playingTimeline.SetActive(true);
		playableDirector = playingTimeline.GetComponent<PlayableDirector> ();
		RestartTimeline();
	}

	public void PlayFinishingGameCutscene()
	{
		HideAllTimelines();
		finishingTimeline.SetActive(true);
		playableDirector = finishingTimeline.GetComponent<PlayableDirector> ();
		RestartTimeline();
	}

	public void PlayNewHighscoreCutscene()
	{
		HideAllTimelines();
		newHighscoreTimeline.SetActive(true);
		playableDirector = newHighscoreTimeline.GetComponent<PlayableDirector> ();
		RestartTimeline();
	}

	public void PlayNewHighscoreFinalCutscene()
	{
		HideAllTimelines();
		newHighscoreFinalTimeline.SetActive(true);
		playableDirector = newHighscoreFinalTimeline.GetComponent<PlayableDirector> ();
		RestartTimeline();
	}
}
