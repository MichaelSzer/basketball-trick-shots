﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeleftText : MonoBehaviour {

	Text text;
	TextMesh textMesh;

	void SetText()
	{
		text = GetComponent<Text> ();

		if(text == null)
			textMesh = GetComponent<TextMesh> ();

		if(text == null && textMesh == null)
			Debug.LogError("UpdateTextCoins script is only for 'Text' or 'TextMesh'");
	}

	void OnStart()
	{
		SetText();

		float timeLeft = ClassicGameBrain.Instance.initialTimer;

		if(text != null)
			text.text = timeLeft.ToString();
		else if(textMesh != null)
			textMesh.text = timeLeft.ToString();
	}
}
