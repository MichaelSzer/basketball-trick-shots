﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateAdsTimer : MonoBehaviour {

	void OnEnable()
	{
		DisplayAdsClassicGameMode.Instance.ActivateAdsTimer();
	}
}
