﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayStartingGameCutscene : MonoBehaviour {

	void OnEnable()
	{
		TimelineManager.Instance.PlayingGameCutscene();
	}
}
