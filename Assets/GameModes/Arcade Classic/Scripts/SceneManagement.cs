﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagement : MonoBehaviour {

	public static SceneManagement Instance;
	public GameObject backConfirmation;

	void Awake()
	{
		if(Instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			Instance = this;
		}
	}

	public void GoToMenu()
	{
		SceneManager.LoadScene("MainMenu");
	}

	public void DisplayBackConfirmation()
	{
		backConfirmation.SetActive(true);
	}

	public void CancelMenu()
	{
		backConfirmation.SetActive(false);
	}
}
