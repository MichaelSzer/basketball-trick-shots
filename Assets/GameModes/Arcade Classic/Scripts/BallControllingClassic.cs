﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BallProperties))]
public class BallControllingClassic : BallControlling {

	bool ShootBrainCallback = false;
	public bool firstBall = false;

	void OnCollisionEnter(Collision other){}
	void OnCollisionExit(Collision other){}

	void Awake()
    {
        lr = GetComponent<LineRenderer> ();
        rb = GetComponent<Rigidbody> ();
        tr = GetComponent<Transform> ();

        InitializeValues();
    }

    void Update()
    {
        if(!ClassicGameBrain.Instance.IsGameStarted())
            lr.enabled = false;

    	if(ClassicGameBrain.Instance.IsTimerStarted() || firstBall)
    		base.Update();
    	
    	if(Shooted() && !ShootBrainCallback)
    	{
    		ShootBrainCallback = true;
    		ClassicGameBrain.Instance.ShootBall();

            if(firstBall)
                ClassicGameBrain.Instance.StartTimer();
    	}
    }

	public bool Shooted()
	{
		return !activated;
	}
}
