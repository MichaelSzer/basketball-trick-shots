﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayFinishingGameCutscene : MonoBehaviour {
	
	void OnEnable()
	{
		TimelineManager.Instance.PlayFinishingGameCutscene();
	}
}
