﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayAdsClassicGameMode : DisplayAdsInTime {

	public static DisplayAdsClassicGameMode Instance;

	bool activate = false;
	
	void Awake()
	{
		if(Instance == null)
		{			
			Instance = this;
		}
		else
		{
			Destroy(gameObject);
		}
	}

	// Update is called once per frame
	void Update () {
		if(activate)
			base.Update();
	}

	public void ActivateAdsTimer()
	{
		//Debug.Log("ActivateAdsTimer()");
		activate = true;
	}

	public void DeactivateAdsTimer()
	{
		//Debug.Log("DeactivateAdsTimer()");
		activate = false;
		FinishPlaying();
	}	
}
