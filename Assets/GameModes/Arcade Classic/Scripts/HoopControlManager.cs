﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Segment
{
	public Vector3 velocity;
	public Vector3 bounds;
}

public class HoopControlManager : MonoBehaviour {

	public static HoopControlManager Instance;

	public List<Segment> segmentA, segmentB, segmentC;

	public bool testSegment = false;
	public int segmentATest, segmentBTest, segmentCTest;

	public float speedToStartPos = 2f;

	Vector3 velocity;
	Vector3 bound;
	Vector3 startedPosition;
	Vector3 side;

	Rigidbody rb;

	private bool returningToStartPos = false;

	// Use this for initialization
	void Awake () {

		if(Instance != null)
		{
			Destroy(this);
		}
		else
		{
			Instance = this;
		}

		rb = GetComponent<Rigidbody> ();
		side = new Vector3(1f, 1f, 1f);
	}

	void Start()
	{
		startedPosition = transform.position;
	}

	void Update()
	{			
		
		if(ClassicGameBrain.Instance.IsTimerStarted() && ClassicGameBrain.Instance.IsGameStarted())
		{
			CheckBoundaries();
		}
		else
		{
			CheckReturnToStartingPosition();
		}


		transform.Translate(new Vector3(0f, Time.deltaTime * velocity.y * side.y, Time.deltaTime * velocity.z * side.z));	

		//Debug.Log(velocity);
	}

	void CheckReturnToStartingPosition()
	{
		if(!returningToStartPos)
			return;

		Vector3 offsetPosition = transform.position - startedPosition;
		//Debug.Log(offsetPosition);

		if(offsetPosition.x < 0f)
		{
			velocity.x = 0f;
		}

		if(offsetPosition.y < 0f)
		{
			velocity.y = 0f;
		}

		if(offsetPosition.z < 0f)
		{
			velocity.z = 0f;
		}

		if(velocity.Equals(new Vector3(0f, 0f, 0f)))
			returningToStartPos = false;
	}

	void CheckBoundaries()
	{
		Vector3 offsetPosition = transform.position - startedPosition;

		if(offsetPosition.z > bound.z)
			side.z = -1;

		if(offsetPosition.y > bound.y)
			side.y = -1;

		if(offsetPosition.z < 0f)
			side.z = 1;

		if(offsetPosition.y < 0f)
			side.y = 1;
	}
	
	// Update is called once per frame
	public void ActivateMoveOne()
	{
		returningToStartPos = false;

		if(testSegment)
		{
			velocity = segmentA[segmentATest].velocity;
			bound = segmentA[segmentATest].bounds;
		}
		else
		{
			int index = Random.Range(0, segmentA.Count);

			velocity = segmentA[index].velocity;
			bound = segmentA[index].bounds;
		}
	}

	public void ActivateMoveTwo()
	{
		if(testSegment)
		{
			velocity = segmentB[segmentBTest].velocity;
			bound = segmentB[segmentBTest].bounds;
		}
		else
		{
			int index = Random.Range(0, segmentB.Count);

			velocity = segmentB[index].velocity;
			bound = segmentB[index].bounds;
		}
	}

	public void ActivateMoveThree()
	{
		if(testSegment)
		{
			velocity = segmentC[segmentCTest].velocity;
			bound = segmentC[segmentCTest].bounds;
		}
		else
		{
			int index = Random.Range(0, segmentC.Count);

			velocity = segmentC[index].velocity;
			bound = segmentC[index].bounds;
		}
	}

	public void ReturnToSpawnPoint()
	{
		//Debug.Log("ReturnToSpawnPoint()");

		// Go backwards to starting position, with side =-1
		side = new Vector3(-1f, -1f, -1f);
		velocity = speedToStartPos * new Vector3(1f, 1f, 1f);

		returningToStartPos = true;
	}

	public void FinishGame()
	{
		ReturnToSpawnPoint();
	}

	public void RestartGame()
	{
		ReturnToSpawnPoint();
	}
}
