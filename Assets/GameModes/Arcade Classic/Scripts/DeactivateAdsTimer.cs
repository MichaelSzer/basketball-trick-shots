﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateAdsTimer : MonoBehaviour {

	void OnEnable()
	{
		DisplayAdsClassicGameMode.Instance.DeactivateAdsTimer();
	}
}
