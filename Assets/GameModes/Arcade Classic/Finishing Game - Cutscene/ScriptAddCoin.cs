﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScriptAddCoin : MonoBehaviour {

	public Text textCoins;

	void OnEnable()
	{
		int coins = PlayerPrefs.GetInt("coins", 0);
		coins++;
		PlayerPrefs.SetInt("coins", coins);
		textCoins.text = coins.ToString();
	}
}
