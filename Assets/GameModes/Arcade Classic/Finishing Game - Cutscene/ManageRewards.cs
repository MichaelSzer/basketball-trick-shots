﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManageRewards : MonoBehaviour {

	private float waitTimeForReward = 0.5f;
	private float timeCoinRewardAnimation = 2f;
	private float timeBetweenCoins = 0.2f;
	public GameObject coinReward;
	public GameObject coinsParent;
	public Text coinsWonText;
	public Text scoreText;

	void OnEnable()
	{
		int score = ClassicGameBrain.Instance.GetScore();
		int coinsWon = (int)(score * RemoteTunningVariables.Instance.ArcadeCoinsPerPoint);

		coinsWonText.text = "+" + coinsWon.ToString();
		scoreText.text = score.ToString();

		StartCoroutine(InstantiateRewardCoins(coinsWon));

		//Debug.Log(score);
	}

	IEnumerator InstantiateRewardCoins(int coins)
	{
		yield return new WaitForSeconds(waitTimeForReward);

		WaitForSeconds waitTimeCoinRewardAnimation = new WaitForSeconds(timeCoinRewardAnimation);
		WaitForSeconds waitTimeBetweenCoins = new WaitForSeconds(timeBetweenCoins);

		while(coins > 0)
		{
			GameObject _coinReward = Instantiate(coinReward, coinsParent.transform);
			_coinReward.SetActive(true);

			coins--;

			//Debug.Log("Coin created!");

			if(coins > 0)
				yield return waitTimeBetweenCoins;
			else
				yield return waitTimeCoinRewardAnimation;
		}

		AnalyticsManager.Instance.TriggerEvent("Reward arcade cutscene finished.");
		DisplayAdsClassicGameMode.Instance.DeactivateAdsTimer();

		TimelineManager.Instance.PlayLobbyCutscene();
	}
}
