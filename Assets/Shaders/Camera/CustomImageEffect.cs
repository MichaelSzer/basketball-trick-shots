﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CustomImageEffect : MonoBehaviour {

	public Material EffectMaterial;

	[Range(0, 10)]
	public int Iterations = 0;

	void OnRenderImage(RenderTexture src, RenderTexture dst)
	{
		dst = new RenderTexture(src.width, src.height, 24);
		Graphics.Blit(src, dst);

		for(int i = 0; i < Iterations; i++)
		{
			RenderTexture rt = new RenderTexture(src.width, src.height, 24);
			Graphics.Blit(dst, rt, EffectMaterial);
			dst.Release();
			dst = rt; 
		}

		//Graphics.Blit(rt, dst);		
		//rt.Release();
	}
}
