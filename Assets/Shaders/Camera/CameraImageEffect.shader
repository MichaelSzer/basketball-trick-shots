﻿Shader "Camera/CustomImageEffect"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_DisplaceTex ("Displacement Texture", 2D) = "white" {}
		_Magnitude ("Magnitude", Range(0, 3)) = 0.02
		_StartingDis ("Starting Displacement", Range(0, 0.2)) = 0
		_TimeSpeed ("Time Speed", Range(0, 2)) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _DisplaceTex;
			half _Magnitude;
			half _StartingDis; 
			half _TimeSpeed;

			fixed4 frag (v2f i) : SV_Target
			{
				half2 delta = half2(_ScreenParams.z - 1, _ScreenParams.w - 1);

				half4 color = half4(0,0,0,0);
				
				// Column 1-2-3
				color += tex2D(_MainTex, fixed2(i.uv.x - delta.x, i.uv.y - delta.y)) + tex2D(_MainTex, fixed2(i.uv.x, i.uv.y - delta.y)) + tex2D(_MainTex, fixed2(i.uv.x + delta.x, i.uv.y - delta.y));
				color += tex2D(_MainTex, fixed2(i.uv.x - delta.x, i.uv.y)) + tex2D(_MainTex, fixed2(i.uv.x, i.uv.y)) + tex2D(_MainTex, fixed2(i.uv.x + delta.x, i.uv.y));
				color += tex2D(_MainTex, fixed2(i.uv.x - delta.x, i.uv.y + delta.y)) + tex2D(_MainTex, fixed2(i.uv.x, i.uv.y + delta.y)) + tex2D(_MainTex, fixed2(i.uv.x + delta.x, i.uv.y + delta.y));
				
				color /= 9;

				return color;
				//float2 disp = tex2D(_DisplaceTex, distuv).xy; 
				//disp = ((disp * 2) - 1) * _Magnitude;

				//fixed4 col = tex2D(_MainTex, i.uv + disp);
				// just invert the colors
				//col *= float4(i.uv.x, i.uv.y, 1, 1);
				//return col;
			}
			ENDCG
		}
	}
}
