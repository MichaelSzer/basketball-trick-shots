﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class StreamVideo : MonoBehaviour {

     public RawImage image;
     public VideoPlayer videoPlayer;
     public AudioSource audioSource;
	  
	// Use this for initialization
	void Start () 
	{
	        StartCoroutine(PlayVideo());
	}
  	
  	IEnumerator PlayVideo()
    {
        videoPlayer.Prepare();
        WaitForSeconds waitForSeconds = new WaitForSeconds(1);
        while (!videoPlayer.isPrepared)
        {
            yield return waitForSeconds;
            break;
        }
    
        image.texture = videoPlayer.texture;
        image.color = new Color(1f, 1f, 1f, 1f);
        videoPlayer.Play();

        if(audioSource != null)
        	audioSource.Play();
     }
}