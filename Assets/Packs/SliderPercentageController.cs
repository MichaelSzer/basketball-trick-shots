﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderPercentageController : MonoBehaviour {    

    public float speedFill = 2f;
    public Slider percentageSlider;
    public Text percentageText;
    public AudioSource sliderAudioSource;
    public AudioClip sliderFilling;

    public delegate void function();

    public void fillSlider(float from, float to, function sliderFinishedCallback)
    {

        percentageSlider.value = from;

        PlaySoundInterval(from * 4, to * 4);
        StartCoroutine(fillSliderCoroutine(to, sliderFinishedCallback));
    }

    void PlaySoundInterval(float fromSeconds, float toSeconds)
    {
        sliderAudioSource.Stop();
        sliderAudioSource.clip = sliderFilling;
        sliderAudioSource.volume = 1;
        sliderAudioSource.time = fromSeconds;
        sliderAudioSource.Play();
        sliderAudioSource.SetScheduledEndTime(AudioSettings.dspTime + (toSeconds - fromSeconds));
    }

    IEnumerator fillSliderCoroutine(float to, function sliderFinishedCallback)
    {
        while (percentageSlider.value < to)
        {
            percentageSlider.value += speedFill * Time.deltaTime;
            percentageText.text = floatToPercentage(percentageSlider.value * 100);
            yield return null;
        }

        percentageSlider.value = to;
        percentageText.text = floatToPercentage(percentageSlider.value * 100);

        // Call callback
        sliderFinishedCallback();
    }

    string floatToPercentage(float percentage)
    {
        // Transform float into a one comma digit number
        percentage = percentage * 10;

        if ((int)percentage > 999)
            return "100%";
        else
            return ((int)percentage / 10).ToString() + "." + ((int)percentage % 10).ToString() + "%";
    }
}
