﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class LoadMenuPack : MonoBehaviour {

    public string cookiePercentageComplete, cookiePercentageCompleteTodo;

    public string pack = "null";

    public bool cityMap = false;
    public bool basketballMap = false;

    public SliderPercentageController sliderPercentageController;
    public GameObject thumbUpImage;

    public bool customCamera;
    public Camera newCamera;
    public Camera mainCamera;

    private SoundtrackManager soundtrackManager;
    public string track;

    public AudioSource audioSource;
    public AudioClip thumbUpSound;

    private void Awake()
    {
        soundtrackManager = GameObject.Find("SoundtrackManager").GetComponent<SoundtrackManager>();
    }

    float percentageCompleted;
    float percentageCompletedTodo;

    void LoadPercentage()
    {
        percentageCompleted = PlayerPrefs.GetFloat(cookiePercentageComplete, (float)0.0);
        percentageCompletedTodo = PlayerPrefs.GetFloat(cookiePercentageCompleteTodo, (float)0.0);

        //sliderPercentageController.fillSlider((float)0.20, (float)0.50);      

        sliderPercentageController.fillSlider(percentageCompleted / 100, percentageCompletedTodo / 100, SliderFinishedCallback);

        if (percentageCompleted == 100)
            thumbUpImage.SetActive(true);
    }


    void SliderFinishedCallback()
    {
        PlayerPrefs.SetFloat(cookiePercentageComplete, percentageCompletedTodo);

        if (percentageCompletedTodo != 100 || percentageCompleted == 100)
            return;

        thumbUpImage.SetActive(true);
        thumbUpImage.GetComponent<Animator>().Play("ThumbWinAnimation");
        PlayThumpUpSound();
    }

    void PlayThumpUpSound()
    {
        audioSource.Stop();
        audioSource.clip = thumbUpSound;
        audioSource.volume = 1;
        audioSource.Play();
    }

    public void StartPack()
    {
        if(pack == "null"){
            Debug.LogError("Name of the pack was not set.");
            return;
        }

        SceneManager.LoadScene(pack);
    }

    private void LoadCamera()
    {
        if(customCamera)
        {
            newCamera.enabled = true;
            mainCamera.enabled = false; 
        }
    }

    private void ShowMap()
    {
        if(cityMap)
            MenuBackgroundManager.Instance.ShowCityMap();
        else if(basketballMap)
            MenuBackgroundManager.Instance.ShowBasketballMap();
    }

    private void UnloadCamera()
    {
        if(customCamera)
        {
            newCamera.enabled = false;
            mainCamera.enabled = true; 
        }
    }

    private void OnEnable()
    {
        LoadCamera();

        ShowMap();

        soundtrackManager.Play(track);
        LoadPercentage();
    }

    private void OnDisable()
    {
        UnloadCamera();
    }
}
