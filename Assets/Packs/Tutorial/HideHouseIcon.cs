﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideHouseIcon : MonoBehaviour {

	void Awake()
	{
        int tutorialCompleted = PlayerPrefs.GetInt("FirstTimeTutorialCompleted", 0);

        if(tutorialCompleted < 1)
        {
        	Destroy(gameObject);
        }
	}
}
